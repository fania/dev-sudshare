$("#PopupAlert").hide(0);
$("#PopupHTML").hide(0);
$("#ConfirmPopup").hide(0);
$("#HypoPopup").hide(0);
$("#PopupAccepted").hide(0);
$("#PopupWeighWarn").hide(0);

var RTdatabase = firebase.database();
var CFdatabase = firebase.firestore();
var FileStorage = firebase.storage();
var storageRef = firebase.storage().ref();
var functions = firebase.functions();
var PageHistory = [];
var MenuOpen = false;
var Loading = false;
var UserID;
var Instance;
var StripeAccountID = "";
var UsersFirstName = "";
var UsersLastName = "";
var UsersPhone = "";
var CustomerOnline = false;
var UsersEmail = "";
var UsersPhone = "";
var UsersAddress = "";
var UsersCity = "";
var UsersState = "";
var UsersZip = "";
var LoadingMinPass = false;
var startPage = 1;
var CurrentPage = "";
var SignupFirstName = "";
var SignupLastName = "";
var LoadingSignup = false;
var SignupPayoutMethod = "DC";
var HTMLcode = "";
var ConfirmIndex = 0;
var MAODataArray = [];
var MAODataIDArray = [];
var BagCounterNumber = 1;
var AllowedPN;
var Platform = "";
var MessageingOrderNumber = "";
var MAONumber = {
  ID: "",
  I: "",
  N: ""
};
var WorkAreaLat = 0;
var StripeAccountID = "";
var SettingsWorkAreaArray;
var AvailableWorkArea;
var SettingsPayoutMethod;
var MAOIndex;
var MAOID;
var MAOBagCounterActive = false;
var geocoder = new google.maps.Geocoder();
var SignupIntervalCounterStart = false;
var UploadedProfileImg = false;
var SettingsProfileImgChanged = false;
var CurrentPageColor;
var TestingMode = false;
var AcceptedOrderName = "";
var AcceptedOrderNumber = "";
var WorkAreaRadius = 0;
var WorkAreaCenter;
var WorkAreaCirlceMap;
var AllowInstantPayout = false;
var PayoutFee = "";
var OrdersArray = [];
var MAOPreAuth = 0;
var OrdersMap;
var TotalBalance = 0;
var GetOrdersBlocked = false;


$(function () {
  if (getURLVariable("platform") == "IOS") {
    $("#BodyScaleDiv").css("box-shadow", "17px");
    Platform = "IOS";
  } else if (getURLVariable("platform") == "Android") {
    Platform = "Android";
  }
  ChangeThemeColor("#25d7fd");
  CFdatabase.doc("Code/WorkArea").get().then(function (doc) {
    AvailableWorkArea = doc.data().Circles;
  });
});
window.addEventListener('offline', function () {
  $("#NoConnection").css("display", "block");
});
window.addEventListener('online', function () {
  $("#NoConnection").css("display", "none");
});

function ToggleLoginStateClick(Button) {
  if ($(Button).html() == "Already have an account? Login") {
    $(Button).html("Don't have an account? Sign Up");
    $("#BottomLoginButton").html("Don't have an account? Sign Up");
    $("#GetStartedLoginDiv").css("display", "block");
    $("#GetStartedSignUpDiv").css("display", "none");
  } else {
    $(Button).html("Already have an account? Login");
    $("#GetStartedLoginDiv").css("display", "none");
    $("#GetStartedSignUpDiv").css("display", "block");
  }
}

function PopupForgotPassword() {
  PopupHTML('<div class="InputDiv"><input id="ForgotPassEmailInput" type="email" placeholder="Email"> <i class="material-icons InputIcon">&#xE0BE;</i> </div> <button onclick="ResetPassword()">Reset Password</button><br>', true);
}

function getURLVariable(variable) {
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split("=");
    if (pair[0] == variable) {
      return pair[1];
    }
  }
  return (false);
}

firebase.auth().onAuthStateChanged(function (user) {
  if (user) {
    // User is signed in.
    $("body").css("background", "white");
    UserID = user.uid;
    CFdatabase.doc("Sudsters/" + UserID).set({
      LastOpened: new Date().getTime()
    }, {
      merge: true
    });
    CFdatabase.doc("Sudsters/" + UserID).get().then(function (doc) {
      if (doc.exists == false) {
        PopupAlert("Account Error", "This account is already registered as a Customer Account. Please signup with a new email address.");
        firebase.auth().signOut();
      }
      if (doc.data().SignupStepNumber == 0) {
        if (doc.data().AccountRejected != true) {
          GoToPage("GetOrders", "Get Orders", true, false, false, true);

          $("#MenuProfileName").html(doc.data().FirstName + " " + doc.data().LastName.charAt(0) + ".");
          $("#MenuProfileSince").html("Sudster Since " + doc.data().SinceMonth);
          //for testing
          /*setTimeout(() => {
            GoToPage("YourMoney", "Your Money", true, false, false, true);
          }, 1000);*/

          //ChangeThemeColor("rgb(35, 35, 35)");

        }
      } else {
        //Still in signup stage
        GoToPage("Signup", "", false, false, false, true);
        $("#SignupBreadcrumb" + (doc.data().SignupStepNumber)).addClass("Done");
        $("#SignupDivHTML").html($("#SignupFrame" + doc.data().SignupStepNumber).html());
        SignupIntervalCounterStart = true;
      }
    });
    CFdatabase.doc("Sudsters/" + UserID).onSnapshot(function (doc) {
      UsersEmail = doc.data().ContactEmail;
      UsersPhone = doc.data().Phone;
      UsersAddress = doc.data().StreetAddress;
      UsersCity = doc.data().City;
      UsersState = doc.data().State;
      UsersZip = doc.data().Zipcode;
      UsersFirstName = doc.data().FirstName;
      UsersLastName = doc.data().LastName;
      StripeAccountID = doc.data().StripeAccountID;
      WorkAreaCenter = doc.data().WorkAreaCenter;
      StripeAccountID = doc.data().StripeAccountID;
      WorkAreaLat = doc.data().AvgWorkAreaLat;
      TotalBalance = doc.data().Balance + doc.data().PendingBalance;
      GetOrdersBlocked = doc.data().BlockGetOrders || false;

      if (TotalBalance >= 1000 && doc.data().AverageRating.Current >= 4 && doc.data().AverageRating.Count >= 4) {
        AllowInstantPayout = true;
        $('#MoneyGetPaidButton').css('opacity', '1');
      }

      if (doc.data().AccountRejected) {
        $("#RejectionParagraph").html(doc.data().AccountRejectedDetails);
        GoToPage("Rejection", "", false, false, false, true);
      } else {
        if (doc.data().SignupStepNumber == 0 && CurrentPage == "Rejection") {
          GoToPage("GetOrders", "Get Orders", true);
        }
      }
      var AverageRatingPercentage = 50;
      if (doc.data().AverageRating != null) {
        AverageRatingPercentage = (doc.data().AverageRating.Current / 5) * 100;
        $("#MenuProfileRatingEdit").html(Math.round(doc.data().AverageRating.Current * 10) / 10);
        if (doc.data().AverageRating.Count == 1) {
          $("#MenuProfileRatingText").css("opacity", "0");
        } else {
          $("#MenuProfileRatingText").css("opacity", "1");
        }
      }
      $("#MenuProfileRatingBar").css("background", "linear-gradient(to right, white " + AverageRatingPercentage + "%, rgba(0,0,0,0) " + (AverageRatingPercentage - 100) + "%)");

      $("#MenuMessageBadge").html(doc.data().MessageBadge);
      $("#MenuMessageBadge").css("display", "block");
      if (doc.data().MessageBadge == 0) {
        $("#MenuMessageBadge").css("display", "none");
      }

      if (WorkAreaLat != null) {
        GetOrderList();
      }

    });

    try {
      window.webkit.messageHandlers.Sudsters.postMessage({
        task: "setuserid",
        userid: UserID
      });
    } catch (e) {}
    try {
      Android.sendUID(UserID);
    } catch (e) {}

  } else {
    // User is signed out.
    GoToPage("Start", "", false, false, false, true);
    $("body").css("background-color", "#25D7FD");
  }
});

function GoToPage(ID, Title, Menu, Back, Backingup, SkipAnimation) {


  if ($("#ScreenSlide").html() == "") {
    CurrentPage = ID;
    if (Title == "") {
      $("header").css("display", "none");
    } else {
      $("#HeaderTitle").html(Title);
      $("header").css("display", "block");
    }
    if (Backingup) {
      $("#ScreenSlide").html($("#Screen").html());
      $("#ScreenSlide").css('left', '0px');
      $("#Screen").html($("#" + ID).html());
      $("#ScreenSlide").animate({
        left: '100vw'
      }, 300, function () {
        $("#ScreenSlide").html("");
      });
      $("#Screen").css("left", "-100px");
      $("#Screen").animate({
        left: '0px'
      }, 300, function () {});
    } else if (SkipAnimation) {
      $("#Screen").html($("#" + ID).html());
    } else {
      $("#ScreenSlide").html($("#" + ID).html());
      $("#ScreenSlide").animate({
        left: '0px'
      }, 300, function () {
        $("#Screen").html($("#ScreenSlide").html());
        $("#ScreenSlide").html("");
        $("#ScreenSlide").css("left", '100vw');
      });
      $("#Screen").animate({
        left: '-100px'
      }, 300, function () {
        $("#Screen").css("left", "0px");
      });
    }
    if (Back) {
      $("#HeaderBackIcon").css("display", "block");
      $("#HeaderMenuIcon").css("display", "none");
      PageHistory.push({
        ID: ID,
        Title: Title,
        Menu: Menu,
        Back: Back
      });
    } else {
      PageHistory.push({
        ID: ID,
        Title: Title,
        Menu: Menu,
        Back: Back
      });
      $("#HeaderBackIcon").css("display", "none");
      $("#HeaderMenuIcon").css("display", "block");
    }
    /*
     if (Menu) {
         $("#HeaderMenuIcon").css("display", "block");
     } else {
         $("#HeaderMenuIcon").css("display", "none");
       }*/
    $("#Screen").animate({
      scrollTop: 0
    }, 00);
  }

}

function ToggleLoadingSignup() {
  if (LoadingSignup) {
    //hide it
    LoadingSignup = false;
    $("#SignupLoadingDiv").css("display", "none").css("opacity", "0");
  } else {
    //show it
    LoadingSignup = true;
    $("#SignupLoadingDiv").css("display", "flex").css("opacity", "1");
  }
}

function ContinueSignUp(Number) {

  if (Number == 1) {
    if ($("#SignupFirstName").val() != "" && $("#SignupLastName").val() != "" && $("#SignupEmail").val() != "" && $("#SignupPassword").val() != "") {
      //sign up user and save name
      ToggleLoadingSignup();
      firebase.auth().createUserWithEmailAndPassword($("#SignupEmail").val(), $("#SignupPassword").val()).then(function (user) {
        // user signed up
        $(".BottomLoginButtonForSignup").css("display", "none");
        SignupFirstName = $("#SignupFirstName").val();
        SignupLastName = $("#SignupLastName").val();
        UsersEmail = $("#SignupEmail").val();
        UserID = user.uid;
        var date = new Date();
        var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];;
        CFdatabase.doc("Sudsters/" + UserID).set({
          FirstName: SignupFirstName,
          LastName: SignupLastName,
          ContactEmail: $("#SignupEmail").val(),
          SignupStepNumber: 2,
          SinceMonth: months[date.getMonth()] + " " + date.getFullYear(),
          AccountRejected: false,
          AccountRejectedDetails: '',
          AlertsOn: false,
          EmailDay: 1
        }).then(function (docRef) {
          $("#SignupBreadcrumb" + (Number + 1)).addClass("Done");
          $("#SignupDivHTML").html($("#SignupFrame2").html());
          setTimeout(function () {
            ToggleLoadingSignup();
          }, 500);
        }).catch(function (error) {
          ToggleLoadingSignup();
          PopupAlert("Error", error.message);
        });
      }).catch(function (error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        ToggleLoadingSignup();
        PopupAlert("Error", errorMessage);
      });
    } else {
      ShakeElement($(".InputDiv"));
    }
  } else if (Number == 2) {
    if ($("#SignupPhone").val() != "" && $("#SignupStreetAddress").val() != "" && $("#SignupZipcode").val() != "" && $("#SignupCity").val() != "" && $("#SignupState").val() != "" && $("#SignupCity").val() != "") {
      ToggleLoadingSignup();
      UsersPhone = $("#SignupPhone").val();
      UsersAddress = $("#SignupStreetAddress").val();
      UsersCity = $("#SignupCity").val();
      UsersState = $("#SignupState").val();
      UsersZip = $("#SignupZipcode").val();

      geocoder.geocode({
        'address': UsersAddress + " " + UsersCity + ", " + UsersState + ", USA"
      }, function (data, status) {


        var InArea = false;
        for (var i = 0; i < AvailableWorkArea.length; i++) {

          if (getDistance(data[0].geometry.location.lat(), data[0].geometry.location.lng(), AvailableWorkArea[i].Center.latitude, AvailableWorkArea[i].Center.longitude) <= AvailableWorkArea[i].Radius) {
            InArea = true;
          }

        }

        if (InArea) {
          CFdatabase.doc("Sudsters/" + UserID).set({
            Phone: $("#SignupPhone").val(),
            StreetAddress: $("#SignupStreetAddress").val(),
            Zipcode: $("#SignupZipcode").val(),
            City: $("#SignupCity").val(),
            State: $("#SignupState").val(),
            SignupStepNumber: 3,
            WorkAreaCenter: new firebase.firestore.GeoPoint(data[0].geometry.location.lat(), data[0].geometry.location.lng()),
            AvgWorkAreaLat: data[0].geometry.location.lat(),
            WorkAreaRadius: 24140
          }, {
            merge: true
          }).then(function (docRef) {
            ToggleLoadingSignup();
            $("#SignupBreadcrumb" + (Number + 1)).addClass("Done");
            $("#SignupDivHTML").html($("#SignupFrame3").html());
          }).catch(function (error) {
            ToggleLoadingSignup();
            PopupAlert("Error", error.message);
          });

        } else {
          ToggleLoadingSignup();
          CFdatabase.doc("Sudsters/" + UserID).set({
            City: $("#SignupCity").val(),
            State: $("#SignupState").val()
          }, {
            merge: true
          });
          PopupAlert("Coming Soon!", "Sorry, we don't service in your area yet. But we'll be there soon!")
        }
      });

    } else {
      ShakeElement($(".InputDiv"));
    }
    /*var fileBlob = URL.createObjectURL(document.getElementById("SignupInputImg").files[0]);
         
    var file = document.getElementById("SignupInputImg").files[0];
         
    var UserID = "testID";
    var storageRef = firebase.storage().ref("Sudster_Selfies/" + UserID);
         
    storageRef.put(file).then(function(snapshot) {
      alert('Uploaded a blob or file!');
    });*/
  } else if (Number == 3) {
    ToggleLoadingSignup();
    if (SignupPayoutMethod == "DC") {
      stripe.createToken(card2, {
        name: UsersFirstName + " " + UsersLastName,
        currency: 'usd'
      }).then(function (result) {
        if (result.error) {
          // Inform the user if there was an error
          ToggleLoadingSignup();
          var errorElement = document.getElementById('card-errors2');
          errorElement.textContent = result.error.message;
          ShakeElement($(".StripeElement"));
        } else {
          stripeTokenHandler(result.token.id);
        }
      });
    } else if (SignupPayoutMethod == "BA") {
      if ($("#SignupRoutingNumber").val() != "" && $("#SignupAccountNumber").val() != "") {
        stripeTokenHandler(null, $("#SignupAccountNumber").val(), $("#SignupRoutingNumber").val());
      } else {
        ToggleLoadingSignup();
        ShakeElement($(".InputDiv"));
      }
    } else {
      ShakeElement("#SignupPayDCDiv");
      ShakeElement("#SignupPayBADiv");
      ToggleLoadingSignup();
    }
  } else if (Number == 4) {
    if ($("#SignupDOB").val().length == 10 && $("#SignupSSN").val().length == 4 && $("#SignupSSN").val() == $("#SignupSSN2").val()) {
      //Seperate DOB to different vars
      var Last4SSNVar = $("#SignupSSN").val();
      var DOBArray = $("#SignupDOB").val().split("/");
      var DOBMonth = parseInt(DOBArray[0], 10);
      var DOBDay = parseInt(DOBArray[1], 10);
      var DOBYear = DOBArray[2];
      var DOBMinAge = 18;
      var DOBdate = new Date();
      DOBdate.setFullYear(DOBYear, DOBMonth - 1, DOBDay);
      var currdate = new Date();
      var setDate = new Date();
      setDate.setFullYear(DOBdate.getFullYear() + DOBMinAge, DOBMonth - 1, DOBDay);
      if ((currdate - setDate) > 0) {
        // you are above 18
        ToggleLoadingSignup();
        CFdatabase.doc("Sudsters/" + UserID).get().then(function (doc) {
          $.post('https://us-central1-laundry-llama.cloudfunctions.net/UpdateStripeSudsterID', {
            Day: DOBDay,
            Month: DOBMonth,
            Year: DOBYear,
            Last4SSN: Last4SSNVar,
            StripeAccountNumber: doc.data().StripeAccountID
          }).always(function (data) {
            if (data.status == 400) {
              //error
              ToggleLoadingSignup();
              PopupAlert("Error", data.responseText);
            } else {
              //Success - move to step 5
              CFdatabase.doc("Sudsters/" + UserID).set({
                SignupStepNumber: 5,
                Balance: 0,
                PendingBalance: 0
              }, {
                merge: true
              }).then(function (docRef) {
                ToggleLoadingSignup();
                $("#SignupBreadcrumb5").addClass("Done");
                $("#SignupDivHTML").html($("#SignupFrame5").html());
                SignupIntervalCounterStart = true;
              }).catch(function (error) {
                ToggleLoadingSignup();
                PopupAlert("Error", error.message);
              });
            }
          });
        });
      } else {
        //below 18
        PopupAlert("Age Limit", "We're sorry but you must be at least 18 years old to be a Sudster. Please register again after your 18th birthday.")
      }
    } else {
      if ($("#SignupDOB").val().length > 1 && $("#SignupDOB").val().length < 10) {
        PopupAlert("DOB Error", "You must enter your DOB in the correct format: MM/DD/YYYY (e.g 02/15/1965).")
      } else if ($("#SignupSSN").val() != $("#SignupSSN2").val() && $("#SignupSSN").val() != "") {
        ShakeElement("#SignupSSN2");
      } else {
        ShakeElement(".InputDiv");
      }
    }
  } else if (Number == 5) {
    //Success - move to step 6
    ToggleLoadingSignup();
    CFdatabase.doc("Sudsters/" + UserID).set({
      SignupStepNumber: 6
    }, {
      merge: true
    }).then(function () {
      ToggleLoadingSignup();
      $("#SignupBreadcrumb6").addClass("Done");
      $("#SignupDivHTML").html($("#SignupFrame6").html());
    }).catch(function (error) {
      ToggleLoadingSignup();
      PopupAlert("Error", error.message);
    });
  } else if (Number == 6) {
    if (Platform == "IOS" && AllowedPN == null) {
      $("#SignupDivHTML").html($("#SignupFrame7").html());
    } else {

      ToggleLoadingSignup();

      $.post('https://us-central1-laundry-llama.cloudfunctions.net/SendSudsterIntroEmail', {
        SudsterName: UsersFirstName,
        SudsterEmail: UsersEmail
      }).always(function (data) {

        CFdatabase.doc("Sudsters/" + UserID).set({
          SignupStepNumber: 0,
          AverageRating: {
            Count: 1,
            Current: 3
          },
          SignupTime: new Date().getTime(),
          AlertsOn: true
        }, {
          merge: true
        }).then(function (docRef) {
          ToggleLoadingSignup();
          PopupAlert("Congratulations!", "You passed the test. Welcome to the SudShare team. Let us know if you have any questions.")
          GoToPage("GetOrders", "Get Orders", true);
          setTimeout(function () {
            GetOrderList();
          }, 300);
          CFdatabase.doc("Sudsters/" + UserID).get().then(function (doc) {
            $("#MenuProfileName").html(doc.data().FirstName + " " + doc.data().LastName.charAt(0) + ".");
            $("#MenuProfileSince").html("Sudster Since " + doc.data().SinceMonth);
            var AverageRatingPercentage = 50;
            if (doc.data().AverageRating != null) {
              AverageRatingPercentage = ((doc.data().AverageRating.Current - 1) / 2) * 100;
              $("#MenuProfileRatingEdit").html(Math.round(doc.data().AverageRating.Current * 10) / 10);
            }
            $("#MenuProfileRatingBar").css("background", "linear-gradient(to right, white " + AverageRatingPercentage + "%, rgba(0,0,0,0) " + (AverageRatingPercentage - 100) + "%)");
            if (doc.data().AverageRating.Count == 1) {
              $("#MenuProfileRatingText").css("opacity", "0");
            } else {
              $("#MenuProfileRatingText").css("opacity", "1");
            }
          });
        }).catch(function (error) {
          ToggleLoadingSignup();
          PopupAlert("Error", error.message);
        });

      });

    }
  }
  $("#Screen").animate({
    scrollTop: 0
  }, 00);
}

function GetProfileURLFromAndroid(URL) {
  $("#SignupPhotoPreviewImg").attr("src", URL);
  $("#SettingsPhotoPreviewImg").attr("src", URL);
  UploadedProfileImg = true;
  SettingsProfileImgChanged = false;
  ToggleLoading();
}

function SignupUpdateProfilePicPreview(files) {
  UploadedProfileImg = true;
  var file = files[0];
  var reader = new FileReader();
  reader.onload = function (e) {
    var img = document.createElement("img");
    img.src = e.target.result;
    img.onload = function () {
      var canvas = document.createElement("canvas");
      var ctx = canvas.getContext("2d");
      ctx.drawImage(img, 0, 0);
      canvas.width = 200;
      canvas.height = 200;
      var xStart = 0;
      var yStart = 0;
      var newHeight = 0;
      var newWidth = 0;
      var aspectRadio = img.height / img.width;
      if (img.height < img.width) {
        //horizontal
        aspectRadio = img.width / img.height;
        newHeight = 200;
        newWidth = aspectRadio * 200;
        xStart = -(newWidth - 200) / 2;
      } else {
        //vertical
        newWidth = 200;
        newHeight = aspectRadio * 200;
        yStart = -(newHeight - 200) / 2;
      }
      var ctx = canvas.getContext("2d");
      ctx.drawImage(img, xStart, yStart, newWidth, newHeight);
      dataurl = canvas.toDataURL(file.type);
      resetOrientation(file, dataurl, function (finalDataUrl) {
        document.getElementById('SignupPhotoPreviewImg').src = finalDataUrl;
      });
    }
  }
  reader.readAsDataURL(file);
}

function SettingsUpdateProfilePicPreview(files) {
  SettingsProfileImgChanged = true;
  var file = files[0];
  var reader = new FileReader();
  reader.onload = function (e) {
    var img = document.createElement("img");
    img.src = e.target.result;
    img.onload = function () {
      var canvas = document.createElement("canvas");
      var ctx = canvas.getContext("2d");
      ctx.drawImage(img, 0, 0);
      canvas.width = 200;
      canvas.height = 200;
      var xStart = 0;
      var yStart = 0;
      var newHeight = 0;
      var newWidth = 0;
      var aspectRadio = img.height / img.width;
      if (img.height < img.width) {
        //horizontal
        aspectRadio = img.width / img.height;
        newHeight = 200;
        newWidth = aspectRadio * 200;
        xStart = -(newWidth - 200) / 2;
      } else {
        //vertical
        newWidth = 200;
        newHeight = aspectRadio * 200;
        yStart = -(newHeight - 200) / 2;
      }
      var ctx = canvas.getContext("2d");
      ctx.drawImage(img, xStart, yStart, newWidth, newHeight);
      dataurl = canvas.toDataURL(file.type);
      resetOrientation(file, dataurl, function (finalDataUrl) {
        document.getElementById('SettingsPhotoPreviewImg').src = finalDataUrl;
      });
    }
  }
  reader.readAsDataURL(file);
}

function SignupPayOptionClick(Element) {
  $("#SignupPayOptionDiv .Selected").removeClass("Selected");
  $(Element).addClass("Selected");
  if ($(Element).html() == "Debit Card") {
    $("#SignupPayBADiv").css("display", "none");
    $("#SignupPayDCDiv").css("display", "block");
    SignupPayoutMethod = "DC";
  } else if ($(Element).html() == "Bank Account") {
    $("#SignupPayBADiv").css("display", "block");
    $("#SignupPayDCDiv").css("display", "none");
    SignupPayoutMethod = "BA";
  }
}
var drawingManager;
var map;
var WorkAreaShapeArray = [];
var SudsterWorkAreaPolygonGlobal = [];
var WorkAreaTooBig = false;

function initMap() {

  CFdatabase.doc("Sudsters/" + UserID).get().then(function (doc) {

    if (doc.data().SignupStepNumber == 3) {
      ToggleLoading();
    }

    geocoder.geocode({
      'address': doc.data().StreetAddress + " " + doc.data().City + " " + doc.data().State
    }, function (data, status) {

      WorkAreaCenter = new firebase.firestore.GeoPoint(data[0].geometry.location.lat(), data[0].geometry.location.lng());

      var PreRadius = 1;

      map = new google.maps.Map(document.getElementById('SignupDrawMap'), {
        center: data[0].geometry.location,
        zoom: 11,
        disableDefaultUI: true
      });

      var marker = new google.maps.Marker({
        position: data[0].geometry.location,
        map: map,
        title: 'Your location'
      });

      WorkAreaCirlceMap = new google.maps.Circle({
        strokeColor: '#25d7fd',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#25d7fd',
        fillOpacity: 0.35,
        map: map,
        center: data[0].geometry.location,
        radius: 8046,
        editable: false,
        draggable: false,
        centerDraggable: false
      });

      WorkAreaCirlceMap.setMap(map);

      if (doc.data().SignupStepNumber == 3) {
        ToggleLoading();
      }

    });

  });

}

function stripeTokenHandler(token, AccountNumberFunc, RoutingNumberFunc) {
  var AccountNumberVar = "";
  var RoutingNumberVar = "";
  var TokenVar = "";
  if (token == null) {
    // Bank Account
    AccountNumberVar = AccountNumberFunc;
    RoutingNumberVar = RoutingNumberFunc;
  } else {
    TokenVar = token;
  }
  $.post('https://us-central1-laundry-llama.cloudfunctions.net/GenerateStripeSudsterID', {
    stripeToken: TokenVar,
    userEmail: UsersEmail,
    userID: UserID,
    payoutMethod: SignupPayoutMethod,
    Address: UsersAddress,
    City: UsersCity,
    State: UsersState,
    Zipcode: UsersZip,
    Phone: UsersPhone,
    FirstName: UsersFirstName,
    LastName: UsersLastName,
    AccountNumber: AccountNumberVar,
    RoutingNumber: RoutingNumberVar,
    Testing: TestingMode
  }).always(function (data) {
    if (data.status == 400) {
      //error
      ToggleLoadingSignup();
      PopupAlert("Error", data.responseText);
    } else {
      //Success - move to step 5

      CFdatabase.doc("Sudsters/" + UserID).set({
        SignupStepNumber: 4
      }, {
        merge: true
      }).then(function (docRef) {
        ToggleLoadingSignup();
        $("#SignupBreadcrumb4").addClass("Done");
        $("#SignupDivHTML").html($("#SignupFrame4").html());
      }).catch(function (error) {
        ToggleLoadingSignup();
        PopupAlert("Error", error.message);
      });
    }
  });
}

function SignupPrefillZip(Zipcode) {
  geocoder.geocode({
    'address': Zipcode
  }, function (data, status) {

    $("#SignupCity").val(data[0].address_components[1].long_name);
    $("#SignupState").val(data[0].address_components[2].short_name)
  });
}

function SettingsPrefillZip(Zipcode) {
  geocoder.geocode({
    'address': Zipcode
  }, function (data, status) {

    $("#SettingsCity").val(data[0].address_components[1].long_name);
    $("#SettingsState").val(data[0].address_components[2].short_name)
  });
}

function ShakeElement(Element) {
  $(Element).addClass("shake animated");
  setTimeout(function () {
    $(Element).removeClass("shake animated");
  }, 1000)
}

function PopupAlert(Title, Paragraph) {
  $("#PopupAlertInnerDiv h1").html(Title);
  $("#PopupAlertInnerDiv h2").html(Paragraph);
  $("#PopupAlert").show();
  $("#PopupAlert").css("opacity", "1");
  $("#PopupAlertInnerDiv").css("margin-top", "0px").css("opacity", "1");
  $("body").css("overflow", "hidden");
  $("#Screen").css("-webkit-filter", "blur(10px)").css("transform", "scale(1.04)");;
  $("header").css("-webkit-filter", "blur(10px)");
}

function PopupHTML(HTML, SmallClose) {
  $("#PopupHTMLClose").removeClass("PopupHTMLCloseSmall");
  $("#PopupHTMLCode").html(HTML);
  $("#PopupHTML").show(0);
  $("#PopupHTML").css("opacity", "1");
  $("#PopupHTMLInnerDiv").css("margin-top", "-50px").css("opacity", "1");
  $("body").css("overflow", "hidden");
  $("#Screen").css("-webkit-filter", "blur(10px)").css("transform", "scale(1.05)");
  $("header").css("-webkit-filter", "blur(10px)");
  if (SmallClose) {
    $("#PopupHTMLClose").addClass("PopupHTMLCloseSmall");
  }
}

function OpenConfirmPopup() {
  $("#ConfirmPopup").show();
  $("#ConfirmPopup").css("opacity", "1");
  $("#ConfirmPopupInnerDiv").css("margin-top", "0px").css("opacity", "1");
  $("body").css("overflow", "hidden");
  $("#Screen").css("-webkit-filter", "blur(10px)").css("transform", "scale(1.05)");
  $("header").css("-webkit-filter", "blur(5px)");
  $("#ConfirmPopupInnerDiv").animate({
    scrollTop: 0
  }, 00);
}

function OpenHypoPopup() {
  $("#HypoPopup").show();
  $("#HypoPopup").css("opacity", "1");
  $("#HypoPopupInnerDiv").css("margin-top", "-50px").css("opacity", "1");
  $("body").css("overflow", "hidden");
  $("#Screen").css("-webkit-filter", "blur(10px)").css("transform", "scale(1.05)");
  $("header").css("-webkit-filter", "blur(10px)");
}

function CloseConfirmPopup(Confirmed) {
  $("body").css("overflow-y", "scroll")
  $("#ConfirmPopup").css("opacity", "0");
  $("#ConfirmPopupInnerDiv").css("margin-top", "200px");
  $("#Screen").css("-webkit-filter", "blur(0px)").css("transform", "none");;
  $("header").css("-webkit-filter", "blur(0px)");
  setTimeout(function () {
    $("#ConfirmPopup").hide(0);
    if (Confirmed) {
      AcceptOrder(ConfirmIndex, true);
    }
  }, 500)
}
$('#ConfirmPopup').on('click', function (e) {
  if (e.target == this) {
    CloseConfirmPopup();
  }
});

function CloseHypoPopup(Confirmed) {
  $("body").css("overflow-y", "scroll")
  $("#HypoPopup").css("opacity", "0");
  $("#HypoPopupInnerDiv").css("margin-top", "200px");
  $("#Screen").css("-webkit-filter", "blur(0px)").css("transform", "none");;
  $("header").css("-webkit-filter", "blur(0px)");
  setTimeout(function () {
    $("#HypoPopup").hide(0);
    if (Confirmed) {
      AcceptOrder(ConfirmID, false, true);
    }
  }, 500)
}

function ClosePopup() {
  $("body").css("overflow-y", "scroll")
  $("#PopupAlert").css("opacity", "0");
  $("#PopupAlertInnerDiv").css("margin-top", "200px");
  $("#Screen").css("-webkit-filter", "blur(0px)").css("transform", "none");;
  $("header").css("-webkit-filter", "blur(0px)");
  setTimeout(function () {
    $("#PopupAlert").hide(0);
  }, 500)
}

function CloseWeighWarnPopup() {
  $("body").css("overflow-y", "scroll")
  $("#PopupWeighWarn").css("opacity", "0");
  $("#PopupWeighWarnInnerDiv").css("margin-top", "200px");
  $("#Screen").css("-webkit-filter", "blur(0px)").css("transform", "none");;
  $("header").css("-webkit-filter", "blur(0px)");
  setTimeout(function () {
    $("#PopupWeighWarn").hide(0);
  }, 500)
}

function ClosePopupHTML() {
  $("body").css("overflow-y", "scroll")
  $("#PopupHTML").css("opacity", "0");
  $("#PopupHTMLInnerDiv").css("margin-top", "200px");
  $("#Screen").css("-webkit-filter", "blur(0px)").css("transform", "none");;
  $("header").css("-webkit-filter", "blur(0px)");
  setTimeout(function () {
    $("#PopupHTML").hide(0);
  }, 500)
}
$("#PopupHTML").click(function () {
  ClosePopupHTML();
});
$("#PopupHTMLInnerDiv").click(function (e) {
  e.stopPropagation();
});

function GoBackPage() {
  var PageIndex = PageHistory.length - 2;
  var PageHistoryItem = PageHistory[PageIndex];
  PageHistory.pop();
  PageHistory.pop();
  GoToPage(PageHistoryItem["ID"], PageHistoryItem["Title"], PageHistoryItem["Menu"], PageHistoryItem["Back"], true);
  if (PageHistoryItem["ID"] == "ManageAO") {
    GetActiveOrderList(MAOID, MAOIndex);
  }
}

function ClickLogin() {
  if ($("#LoginEmail").val() != "" && $("#LoginPassword").val() != "") {
    ToggleLoading();
    firebase.auth().signInWithEmailAndPassword($("#LoginEmail").val(), $("#LoginPassword").val()).then(function (user) {
      // user signed up
      ToggleLoading();

      try {
        window.webkit.messageHandlers.Sudsters.postMessage({
          task: "AskForPN",
          userid: firebase.auth().currentUser.uid
        });
      } catch (e) {}

    }).catch(function (error) {
      // Handle Errors here.
      ToggleLoading();
      var errorCode = error.code;
      if (errorCode == "auth/invalid-email") {
        PopupAlert("Invalid Email", "You entered an invalid email address. Please correct your mistake.");
      } else if (errorCode == "auth/user-not-found") {
        PopupAlert("Invalid Email", "Either you entered the wrong email or you don’t have an account. Enter the email address you used to sign up or create a new account.");
      } else if (errorCode == "auth/wrong-password") {
        PopupAlert("Wrong Password", "The password you entered is incorrect. Please enter the correct password or reset your password.");
      } else {
        PopupAlert(errorCode, error.message);
      }
    });
  } else {
    $(".InputDiv").addClass("shake animated");
    setTimeout(function () {
      $(".InputDiv").removeClass("shake animated");
    }, 1000)
  }
}

function ResetPassword() {
  if ($("#ForgotPassEmailInput").val() != "") {
    var auth = firebase.auth();
    var emailAddress = $("#ForgotPassEmailInput").val();
    auth.sendPasswordResetEmail(emailAddress).then(function () {
      // Email sent.
      ClosePopupHTML();
      PopupAlert("Email Sent", "You should receive an email shortly to reset your password. Please contact us if you have any difficulties.");
    }, function (error) {
      // An error happened.
      var errorCode = error.code;
      var errorMessage = error.message;
      PopupAlert("Error", errorMessage);
    });
  } else {
    $("#PopupHTML .InputDiv").addClass("shake animated");
    setTimeout(function () {
      $(".InputDiv").removeClass("shake animated");
    }, 1000)
  }
}

function ClickMenu() {
  if (MenuOpen) {
    //Close Menu
    $("body").css("background", "white");
    $('#BodyScaleDiv').removeClass('BodyScaleDivOpen');
    $('#BodyScaleDiv').addClass('BodyScaleDivClose');
    $("header").css("border-radius", "0px");
    $("#Screen").css("border-radius", "0px");
    $("#BodyScaleDiv").css("border-radius", "0px");
    MenuOpen = false;
  } else {
    //open menu
    $("body").css("background", CurrentPageColor);
    $('#BodyScaleDiv').addClass('BodyScaleDivOpen');
    $('#BodyScaleDiv').removeClass('BodyScaleDivClose');
    $("header").css("border-top-left-radius", "10px");
    $("#Screen").css("border-radius", "10px");
    $("#BodyScaleDiv").css("border-radius", "10px");
    MenuOpen = true;
  }
}
$("#Screen").click(function () {
  if (MenuOpen) {
    //Close Menu
    $("body").css("background", "white");
    $('#BodyScaleDiv').removeClass('BodyScaleDivOpen');
    $('#BodyScaleDiv').addClass('BodyScaleDivClose');
    $("header").css("border-radius", "0px");
    $("#Screen").css("border-radius", "0px");
    $("#BodyScaleDiv").css("border-radius", "0px");
    MenuOpen = false;
  }
});

function ToggleLoading(AddCheck) {
  if (Loading) {
    if (AddCheck) {
      $("#LoadingDiv .loader").css("display", "none");
      $("#LoadingDiv i").css("opacity", "1").css("transform", "scale(1)");
      setTimeout(function () {
        $("#LoadingDiv").css("display", "none").css("opacity", "0");
        Loading = false;
        $("#LoadingDiv .loader").css("display", "block");
        $("#LoadingDiv i").css("opacity", "0").css("transform", "scale(0)");
      }, 1500);
    } else {
      $("#LoadingDiv").css("display", "none").css("opacity", "0");
      Loading = false;
    }
  } else {
    $("#LoadingDiv").css("display", "flex").css("opacity", "1");
    Loading = true;
  }
}

function addDashesPhone(f) {
  f.value = f.value.replace(/(\d{3})(\d{3})(\d+)/, '$1-$2-$3');
}

function formatAMPM(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'PM' : 'AM';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0' + minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
}

function ClickMenuItem(TabNumber, Element) {
  $("#Screen").animate({
    scrollTop: 0
  }, 00);
  $("#HeaderExtraButton").css("display", "none");
  ClickMenu();
  $(".MenuItemLarge").removeClass('MenuItemSelected');
  $(".MenuItemSmall").removeClass('MenuItemSelected');
  $(Element).addClass('MenuItemSelected');
  CurrentPage = TabNumber;
  if (TabNumber == 1) {
    GoToPage("GetOrders", "Get Orders", true, false, false, true);
    ChangeThemeColor("#25d7fd");
    GetOrderList();
  } else if (TabNumber == 2) {
    GoToPage("ActiveOrders", "Active Orders", true, false, false, true);
    ChangeThemeColor("#fec42f");
    GetActiveOrderList();
  } else if (TabNumber == 3) {
    GoToPage("OrderHistory", "Order History", true, false, false, true);
    ChangeThemeColor("#fd25d7");
  } else if (TabNumber == 4) {
    GoToPage("YourMoney", "Your Money", false, false, false, true);
    ChangeThemeColor("#21e3a4");
  } else if (TabNumber == 5) {
    GoToPage("Help", "Help", true, false, false, true);
    ChangeThemeColor("rgb(35, 35, 35)");
  } else if (TabNumber == 6) {
    GoToPage("Settings", "Settings", true, false, false, true);
    ChangeThemeColor("rgb(35, 35, 35)");
  } else if (TabNumber == 7) {
    GoToPage("Resources", "Resources", false, false, false, true);
    ChangeThemeColor("#FFC107");
  }
}

function AddExtraMenuButton(Icon, Function) {
  $("#HeaderExtraButton").css("display", "block");
  $("#HeaderExtraButton").attr("onclick", Function)
  $("#HeaderExtraButton").html(Icon);

}

function snapshotToArray(snapshot) {
  var returnArr = [];
  snapshot.forEach(function (childSnapshot) {
    var item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });
  return returnArr;
};

function ChangePassword() {
  ToggleLoading();
  firebase.auth().sendPasswordResetEmail(UsersEmail).then(function () {
    // Email sent.
    ToggleLoading();
    PopupAlert("Reset Email Sent", "Please check your emails. You should receive an email shortly to reset your password. The email has been sent to " + UsersEmail)
  }).catch(function (error) {
    // An error happened.
    ToggleLoading();
    PopupAlert("Reset Email Error", error.message);
  });
}

function CheckingChangesToDatabase() {}

function CheckForMessages() {

  ToggleLoading();

  setTimeout(() => {

    CFdatabase.doc("Orders/" + MAONumber.ID).update({
      SudsterMessageBadge: 0
    }).catch(function (e) {});
    $("#MAOMessageBadge").css("display", "none");
    MAODataArray[MAONumber.I].MessageBadge = 0;

  }, 500);

  $("#MessageInput").attr("placeholder", "Type Message");

  CFdatabase.collection("Orders/" + MAONumber.ID + "/Messages").onSnapshot(function (Messages) {

    $("#MessageListDiv").html('');

    Messages.forEach(function (Message) {

      var MessageClass = "MessageItemWasher";
      if (Message.data().Name != UsersFirstName) {
        MessageClass = "MessageItemCustomer";
      }
      $("#MessageListDiv").append('<span class="MessageBreak"></span><p class="' + MessageClass + '"><label>' + EHTML(Message.data().Name) + ' - ' + EHTML(Message.data().Time) + '</label>' + EHTML(Message.data().Text) + '</p>');

    });


    $("#MessageListDiv").animate({
      scrollTop: $("#MessageListDiv")[0].scrollHeight
    }, 100);

  });

}

function ClickSendMessage() {
  if ($("#MessageInput").val() != "") {

    var MessageText = $("#MessageInput").val();


    var messageObject = {}
    messageObject[new Date().getTime()] = {
      Text: MessageText,
      Name: UsersFirstName,
      Time: formatAMPM(new Date()),
      Customer: false
    };

    $("#MessageInput").val("Sending...");

      //Central Order Hub START
      var DaysOfWeek = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"]
      CFdatabase.doc('Orders/' + MAONumber.ID + "/Messages/" + new Date().getTime()).set({
        Text: MessageText,
        Name: UsersFirstName,
        Time: DaysOfWeek[new Date().getDay()] + " " + formatAMPM(new Date())
      }).then(function () {

      }).catch(function (error) {
        console.log("COH Error -", error);
      });
      //Central Order Hub END


      $("#MessageInput").val("");
      $("#MessageListDiv").animate({
        scrollTop: $("#MessageListDiv")[0].scrollHeight
      }, 1000);



  }

}

function GoToMessages(OrderNumber) {
  CFdatabase.doc("Sudsters/" + UserID + "/ActiveOrders/" + OrderNumber).set({
    MessageBadge: 0
  }, {
    merge: true
  });
  $("#MAOMessageBadge").css("display", "none");
  MAODataArray[MAOIndex].MessageBadge = 0;
  GoToPage("Messages", "Messages", false, true, false);
  MessageingOrderNumber = OrderNumber;
}

function LoadSettingsInfo() {

  ToggleLoading();

  CFdatabase.doc("Sudsters/" + UserID).get().then(function (doc) {

    $("#SettingsFirstName").val(doc.data().FirstName);
    $("#SettingsLastName").val(doc.data().LastName);
    $("#SettingsEmail").val(doc.data().ContactEmail);
    $("#OrderAlertsSwitchInput").attr('checked', doc.data().AlertsOn);
    $("#SettingsPhone").val(doc.data().Phone);
    $("#SettingsStreetAddress").val(doc.data().StreetAddress);
    $("#SettingsZipcode").val(doc.data().Zipcode);
    $("#SettingsCity").val(doc.data().City);
    $("#SettingsState").val(doc.data().State);
    $("#SettingsPhotoPreviewImg").attr("src", doc.data().ProfileImgLink);
    $("#SettingsPayOptionPreview span").html(doc.data().PayoutMethod);
    WorkAreaRadius = doc.data().WorkAreaRadius;
    $("#SettingsWorkAreaRadiusInput").val((Math.round((WorkAreaRadius / 1609) * 10) / 10))
    /* var SettingsMap = new google.maps.Map(document.getElementById('SettingDrawMap'), {
        center: {lat: WorkAreaCenter.latitude, lng: WorkAreaCenter.longitude},
        zoom: 11,
        disableDefaultUI: true
      });
      
      var marker = new google.maps.Marker({
        position: {lat: WorkAreaCenter.latitude, lng: WorkAreaCenter.longitude},
        map: SettingsMap,
        title: 'Your location',
        
      });
      
      var WorkCircle = new google.maps.Circle({
        strokeColor: '#25d7fd',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#25d7fd',
        fillOpacity: 0.35,
        map: SettingsMap,
        center: {lat: WorkAreaCenter.latitude, lng: WorkAreaCenter.longitude},
        radius: WorkAreaRadius,
        editable: true,
        draggable: false,
        centerDraggable: false
      });
      
      google.maps.event.addListener(WorkCircle, 'radius_changed', function() {
        
        var Radius = WorkCircle.getRadius();
        
        WorkAreaRadius = parseInt(Radius);
        
        if (Radius > 32187) {
          $("#MapError").css("display", "block");
          WorkAreaTooBig = true;
        } else {
          $("#MapError").css("display", "none");
          WorkAreaTooBig = false;
        }
        
        $("#SignupWorkRadiusText").html("Work Radius: " + (Math.round((Radius / 1609) * 10) / 10) + " Miles")
        
              
        
        
      });
      
      google.maps.event.addListener(WorkCircle, 'center_changed', function() {setTimeout(function(){WorkCircle.setCenter({lat: WorkAreaCenter.latitude, lng: WorkAreaCenter.longitude});},100);});

      WorkCircle.setMap(SettingsMap);
     
     
      ToggleLoading();

      $("#Screen").animate({
        scrollTop: 0
      }, 00);
    
      */

    ToggleLoading();

    $("#Screen").animate({
      scrollTop: 0
    }, 00);

  });


}

function SaveSettingChanges() {
  ToggleLoading();

  CFdatabase.doc("Sudsters/" + UserID).update({
    FirstName: $("#SettingsFirstName").val(),
    LastName: $("#SettingsLastName").val(),
    Phone: $("#SettingsPhone").val(),
    ContactEmail: $("#SettingsEmail").val(),
    StreetAddress: $("#SettingsStreetAddress").val(),
    Zipcode: $("#SettingsZipcode").val(),
    State: $("#SettingsState").val(),
    City: $("#SettingsCity").val(),
    AlertsOn: document.getElementById("OrderAlertsSwitchInput").checked,
    WorkAreaRadius: WorkAreaRadius
  }).then(function () {

    if (SettingsWorkAreaArray != null || SettingsPayoutMethod != null || SettingsProfileImgChanged) {

      if (SettingsWorkAreaArray != null) {
        CFdatabase.doc("Sudsters/" + UserID).update({
          WorkArea: JSON.stringify(SettingsWorkAreaArray)
        });
      }

      if (SettingsPayoutMethod != null) {

        if (SettingsPayoutMethod == "DC") {
          stripe.createToken(card3, {
            name: UsersFirstName + " " + UsersLastName,
            currency: 'usd'
          }).then(function (result) {
            if (result.error) {
              // Inform the user if there was an error
              ToggleLoading();
              var errorElement = document.getElementById('card-errors3');
              errorElement.textContent = result.error.message;
              ShakeElement($(".StripeElement"));
            } else {
              SettingsTokenHandler(result.token.id);
            }
          });
        } else if (SettingsPayoutMethod == "BA") {

          if ($("#SettingsRoutingNumber").val() != "" && $("#SettingsAccountNumber").val() != "") {
            SettingsTokenHandler(null, $("#SettingsAccountNumber").val(), $("#SettingsRoutingNumber").val());
          } else {
            ToggleLoading();
            ShakeElement($(".SettingsPayBAInputDiv"));
          }

        }

      }

      if (SettingsProfileImgChanged) {

        if (Platform == "Android") {
          CFdatabase.doc("Sudsters/" + UserID).set({
            ProfileImgLink: $("#SettingsPhotoPreviewImg").attr("src")
          }, {
            merge: true
          }).then(function (docRef) {
            ToggleLoading(true);
          }).catch(function (error) {
            ToggleLoading(true);
            PopupAlert("Error", error.message);
          });
        } else {
          var storageRef = firebase.storage().ref("Sudster_Selfies/" + UserID);
          storageRef.putString($("#SettingsPhotoPreviewImg").attr("src"), 'data_url').then(function (snapshot) {
            CFdatabase.doc("Sudsters/" + UserID).set({
              ProfileImgLink: snapshot.downloadURL
            }, {
              merge: true
            }).then(function (docRef) {
              ToggleLoading(true);
            }).catch(function (error) {
              ToggleLoading(true);
              PopupAlert("Error", error.message);
            });
          }).catch(function (error) {
            ToggleLoading(true);
            PopupAlert("Error", error.message);
          });
        }

      }

    } else {
      ToggleLoading(true);
    }




  });

}

function SettingsTokenHandler(token, AccountNumberFunc, RoutingNumberFunc) {
  var AccountNumberVar = "";
  var RoutingNumberVar = "";
  var TokenVar = "";
  if (token == null) {
    // Bank Account
    AccountNumberVar = AccountNumberFunc;
    RoutingNumberVar = RoutingNumberFunc;
  } else {
    TokenVar = token;
  }
  $.post('https://us-central1-laundry-llama.cloudfunctions.net/ChangeSudsterPayout', {
    stripeToken: TokenVar,
    userID: UserID,
    payoutMethod: SettingsPayoutMethod,
    Address: UsersAddress,
    City: UsersCity,
    State: UsersState,
    Zipcode: UsersZip,
    FirstName: UsersFirstName,
    LastName: UsersLastName,
    AccountNumber: AccountNumberVar,
    RoutingNumber: RoutingNumberVar,
    Testing: TestingMode
  }).always(function (data) {
    if (data.status == 400) {
      ToggleLoading();
      PopupAlert("Error", data.responseText);
    } else {
      SettingsPayoutMethod = null;
      $('#SettingsPayOptionPreview').css('display', 'block');
      $('#SettingsPayOptionBlur').addClass('Blur');
      $("#SettingsPayOptionPreview span").html(data.responseText);
      ToggleLoading(true);
    }
  });
}

function SubmitContactEmail() {
  if ($("#ContactTextArea").val() != "") {
    ToggleLoading();
    $.ajax({
      url: "https://us-central1-laundry-llama.cloudfunctions.net/SendUsHelpEmail",
      method: "POST",
      data: {
        name: UsersFirstName,
        userid: UserID,
        email: UsersEmail,
        message: $("#ContactTextArea").val(),
        sudster: "TRUE"
      },
    }).fail(function (data, status) {
      //DONE
      ToggleLoading(true);
      PopupAlert("SENT", "Your message has been sent.<br>We will get back to you shortly.");
      $("#ContactTextArea").val("");
    });
  } else {
    ShakeElement($("#ContactTextArea"));
  }
}

function ChangeStartPage(PageNumber, Back) {
  $(".StartFrameInner1_5Div").css('display', "block");
  $("#StartFrameInner6Div").css('display', "none");
  startPage = PageNumber;
  var Title = "";
  var Subtitle = "";
  var BackgroundColor = "";
  var StartIcon = "";
  if (PageNumber == 1) {
    Title = "Work from Home";
    Subtitle = "Earn Money Doing Laundry<br>";
    BackgroundColor = "#25D7FD";
    StartIcon = "E54A";
  }
  if (PageNumber == 2) {
    Title = "Set Your Hours";
    Subtitle = "You're the boss. Work when you want. Earn what you need.";
    BackgroundColor = "#ffc107";
    StartIcon = "E8B5";
  }
  if (PageNumber == 3) {
    Title = "Feel Good";
    Subtitle = "Your work saves people time<br>and makes their life easier";
    BackgroundColor = "#21E3A4";
    StartIcon = "E420";
  }
  if (PageNumber == 4) {
    Title = "Get Paid Instantly";
    Subtitle = "Use Instant Payouts to get paid within minutes.";
    BackgroundColor = "#FD25D7";
    StartIcon = "E227";
  }
  if (PageNumber == 5) {
    Title = "Start Right Away";
    Subtitle = "Getting started is easy.<br>Sign up > Video Training > Get Work";
    BackgroundColor = "#262626";
    StartIcon = "E913";
  }
  $("#StartSignUpButton").css("color", BackgroundColor);
  if (PageNumber == 6) {
    Title = "";
    Subtitle = "";
    BackgroundColor = "white";
    $("#StartSignUpButton").css("color", "#262626");
  }
  BackgroundSlidingColor("#StartInnerDiv", BackgroundColor, Back);
  $("#StartPageDots b").removeClass("StartDotsSelected");
  $("#StartPageDots b:nth-child(" + PageNumber + ")").addClass("StartDotsSelected");
  $("#StartCenterDiv").css("opacity", "0");
  setTimeout(function () {
    if (PageNumber == 6) {
      $(".StartFrameInner1_5Div").css('display', "none");
      $("#StartFrameInner6Div").css('display', "block");
    }
    $("#StartIcon").html("&#x" + StartIcon + ";");
    $("#StartTitle").html(Title);
    $("#StartSubtitle").html(Subtitle);
    $("#StartCenterDiv").css("opacity", "1");
  }, 250)
}

function SettingsPayOptionClick(Element) {
  $(".SettingPayOptionDiv .Selected").removeClass("Selected");
  $(Element).addClass("Selected");
  if ($(Element).html() == "Debit Card") {
    $("#SettingsPayBADiv").css("display", "none");
    $("#SettingsPayDCDiv").css("display", "block");
    SettingsPayoutMethod = "DC";
  } else if ($(Element).html() == "Bank Account") {
    $("#SettingsPayBADiv").css("display", "block");
    $("#SettingsPayDCDiv").css("display", "none");
    SettingsPayoutMethod = "BA";
  }
}

function NextStartPage() {
  if (startPage <= 5) {
    startPage++;
    ChangeStartPage(startPage);
    $("#StartSwipeLine").css("display", "none");
  }
}

function BackStartPage() {
  if (startPage >= 2) {
    startPage--;
    ChangeStartPage(startPage, true);
  }
}

function BackgroundSlidingColor(Element, NextColor, Back) {
  $("body").css("background-color", NextColor);
  var OriganalColor = $(Element).css("background-color") + "";
  $(Element).css("background", "linear-gradient(to right, " + OriganalColor + " 50%, " + NextColor + " 50%)").css('background-size', "200%")
  var DirectionMinus = "-"
  if (Back) {
    DirectionMinus = "";
  }
  $(Element).animate({
    backgroundPosition: DirectionMinus + "100vw"
  }, 500, function () {
    $(Element).css("background-position", "0").css("background", NextColor);
  });
}

function ChangeThemeColor(Color) {
  $("#HeaderTitle").css("color", shadeColor(Color, -5));
  $("#HeaderMenuIcon").css("color", shadeColor(Color, -5));
  $("#HeaderBackIcon").css("color", shadeColor(Color, -5));
  if (UserID != null) {
    $("body").css("background-color", "white");
  }
  $(".MenuItemLarge").css("color", "white");
  $(".MenuItemSmall").css("color", "white");
  $(".MenuItemSelected").css("color", Color);
  $("#MenuDiv").css("background-color", Color);
  CurrentPageColor = Color;
}

function hexToRgbA(hex, Alpha) {
  var c;
  if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
    c = hex.substring(1).split('');
    if (c.length == 3) {
      c = [c[0], c[0], c[1], c[1], c[2], c[2]];
    }
    c = '0x' + c.join('');
    return 'rgba(' + [(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',') + ',' + Alpha + ')';
  }
  throw new Error('Bad Hex');
}

function shadeColor(color, percent) { // deprecated. See below.
  var num = parseInt(color.slice(1), 16),
    amt = Math.round(2.55 * percent),
    R = (num >> 16) + amt,
    G = (num >> 8 & 0x00FF) + amt,
    B = (num & 0x0000FF) + amt;
  return "#" + (0x1000000 + (R < 255 ? R < 1 ? 0 : R : 255) * 0x10000 + (G < 255 ? G < 1 ? 0 : G : 255) * 0x100 + (B < 255 ? B < 1 ? 0 : B : 255)).toString(16).slice(1);
}

function FAQPPClick() {
  PopupHTML($("#FAQDiv").html());
}

function HIWPPClick() {
  PopupHTML('<div id="HIWPPDiv"><h1 id="HIWPPTitle">How It Works</h1> <div class="HIWPPListItem"> <h2 style="color: #25fdb7"><span style="background-color: #25fdb7">1</span> Sign-Up</h2> <h3>Simply fill out the form and watch a short basic training video.</h3> </div> <div class="HIWPPListItem"> <h2 style="color: #ffc107"><span style="background-color: #ffc107">2</span> Accept Jobs</h2> <h3>We send you jobs in your area. Accept only the jobs you want.</h3> </div> <div class="HIWPPListItem"> <h2 style="color: #fd25d7"><span style="background-color: #fd25d7">3</span> Wash-Dry-Fold</h2> <h3>Pick up your customers laundry, wash-dry-fold, and return it within 24 hours.</h3> </div> <div class="HIWPPListItem"> <h2 style="color: #25d7fd"><span style="background-color: #25d7fd">4</span> Get Paid</h2> <h3>Use Instant Payouts to get paid within minutes.</h3> </div> </div>');
  $("#IntroHIWButton").css("display", "none");
}

function RequirementsPP() {
  PopupAlert("", "<h1 style='line-height:110%;font-size:22px;margin-top:-35px;'>Signup Requirements</h1><ul style='text-align:left;font-size:12px;line-height:120%;margin-top:15px;'><li><span>Last 4 of Social Security Number (for background check)</span></li><li><span>Bank Account or Debit Card (for Direct Deposit of Your Earnings)</span></li><li><span>At least 18 years old</span></li><li><span>Access to a washer &amp; dryer</span></li><li><span>A car or some way to pick-up and deliver</span></li></ul> <h1 style='line-height:110%;font-size:22px'>Required Supplies</h1> <ul style='text-align:left;font-size:12px;line-height:120%;'><li><span>Laundry Detergent</span></li><li><span>Clear trash bags to pack folded laundry</span></li><li><span>Stickers for labeling laundry bags</span></li><li><span>Scale to weigh clothes</span></li><li><span>Laundry Baskets</span></li></ul>");
}

function CancelGetOrderItem(ParentID) {
  setTimeout(function () {
    $("#GetOrderItem-" + ParentID).removeClass("GetOrderItemCheckmarkOn");
  }, 100);
}

function InitOrdersMap(CenterLat, CenterLng, Radius) {
  OrdersMap = new google.maps.Map(document.getElementById('GetOrdersMap'), {
    center: new google.maps.LatLng(CenterLat - 0.03, CenterLng),
    mapTypeId: 'roadmap',
    mapTypeControl: false,
    maxZoom: 13,
    minZoom: 8,
    scaleControl: false,
    streetViewControl: false,
    zoom: 10,
    backgroundColor: "#f8f8f8",
    disableDefaultUI: true,
    fullscreenControl: false,
    styles: [{
        elementType: 'all',
        stylers: [{
          visibility: 'simplified'
        }]
      },
      {
        featureType: 'road.highway',
        elementType: 'geometry',
        stylers: [{
          color: '#f1f1f1'
        }]
      },
      {
        featureType: 'road.highway',
        elementType: 'labels',
        stylers: [{
          saturation: '-100'
        }, {
          gamma: '3'
        }]
      },
      {
        featureType: 'road.local',
        stylers: [{
          visibility: 'off'
        }]
      },
      {
        featureType: 'road.arterial',
        elementType: 'geometry',
        stylers: [{
          color: '#f9f9f9'
        }]
      },
      {
        featureType: 'road.arterial',
        elementType: 'labels',
        stylers: [{
          visibility: 'off'
        }]
      },
      {
        featureType: 'water',
        stylers: [{
          color: '#e9fbfe'
        }]
      },
      {
        featureType: 'landscape',
        stylers: [{
          color: '#FFFFFF'
        }]
      },
      {
        featureType: 'poi',
        stylers: [{
          visibility: 'off'
        }]
      },
      {
        featureType: 'transit',
        stylers: [{
          visibility: 'off'
        }]
      },
      {
        featureType: 'administrative',
        stylers: [{
          color: '#b7b7b7'
        }]
      },
      {
        featureType: 'administrative.locality',
        stylers: [{
          color: '#727272'
        }]
      },
    ]
  });

  var NonStyle = [{
    featureType: 'road',
    elementType: 'labels',
    stylers: [{
      visibility: 'off'
    }]
  }, ]

  /*var cityCircle = new google.maps.Circle({
    strokeColor: '#000000',
    strokeOpacity: 0.2,
    strokeWeight: 1,
    fillColor: 'none',
    fillOpacity: 0,
    map: OrdersMap,
    center: new google.maps.LatLng(CenterLat,CenterLng),
    radius: Radius
  });*/

  var marker = new google.maps.Marker({
    position: new google.maps.LatLng(CenterLat, CenterLng),
    map: OrdersMap,
    zIndex: 1000,
    icon: 'https://firebasestorage.googleapis.com/v0/b/laundry-llama.appspot.com/o/Assets%2FHomeMapIcon.png?alt=media&token=48f6dc62-9b98-4060-ac13-ad2b90c7422c'
  });

}

function GetOrderList() {

  if (GetOrdersBlocked == false) {
    setTimeout(function () {
      CFdatabase.collection("Orders").where("SudsterID", "==", UserID).where("Active", "==", true).get().then(function (AOCollection) {

        AOCollection.forEach(function (ActiveOrderDoc) {

          var MarkerLabel = ActiveOrderDoc.data().CustomerFirstName;
          MarkerLabel = MarkerLabel.substring(0, 10);
          var marker = new google.maps.Marker({
            position: new google.maps.LatLng(ActiveOrderDoc.data().Lat, ActiveOrderDoc.data().Lng),
            map: OrdersMap,
            label: {
              text: MarkerLabel,
              color: "#FFFFFF",
              fontSize: "14px",
              fontFamily: "Work Sans"
            },
            icon: {
              labelOrigin: new google.maps.Point(38, 12),
              url: "https://firebasestorage.googleapis.com/v0/b/laundry-llama.appspot.com/o/Assets%2FOrderMapIconOrange.png?alt=media&token=7b915449-125c-4a3b-9253-e0bd3770c66c"
            }
          });

          google.maps.event.addListener(marker, "click", function (e) {
            ClickMenuItem(2, $('#MenuActiveOrderNav'));
            ClickMenu();
          });

        });

        if (AOCollection.docs.length != 0) {
          $("#GetOrdersAONumber").html(AOCollection.docs.length);
          $("#GetOrdersAOLAlert").css("opacity", "0").css("bottom", "-40px");
          setTimeout(function () {
            $("#GetOrdersAOLAlert").css("opacity", "1").css("bottom", "0px");
          }, 300);

        }

      });
    }, 500);

    AddExtraMenuButton("&#xE5D5;", "GetOrderList();$(this).addClass('RotateIcon');");
    $("#GetOrdersLoading").css('display', "block").html("Loading...");

    //narrow down area of sudster with Latitude (within 55 miles)
    //Get average lat/lng point in work area
    var MaxMinLat = {
      min: (WorkAreaLat - 0.55),
      max: (WorkAreaLat + 0.55)
    };

    CFdatabase.collection("Orders").where("Accepted", "==", false).where("Lat", ">=", MaxMinLat.min).where("Lat", "<=", MaxMinLat.max).get().then(function (snapshot) {
      OrdersArray = [];
      var OutsideOrdersArray = [];
      var NoOrders = true;
      $("#GetOrdersHTMLDiv").html("");
      var Counter = 0;

      snapshot.forEach(function (doc) {
        //check every order to see if it fit in sudster work area
        //if (doc.data().FirstName != "Testing" || UserID == "2MfMVQSUCiaxhvP8GECGEgQTYHw2") {

        var DistanceToSudster = getDistance(doc.data().Lat, doc.data().Lng, WorkAreaCenter.latitude, WorkAreaCenter.longitude) * 1000; //*1000 to convert km to m

        NoOrders = false;
        var TimeLeft24 = (24 - parseInt((new Date().getTime() - doc.data().TimeStamp) / (1000 * 60 * 60)));
        if (TimeLeft24 <= 0) {
          TimeLeft24 = 0;
        }
        var NumberOfBags = doc.data().OrderSize;
        var service = new google.maps.DistanceMatrixService();
        service.getDistanceMatrix({
          origins: [UsersAddress + ", " + UsersCity + ", " + UsersState],
          destinations: [doc.data().Address],
          travelMode: 'DRIVING'
        }, function callback(response, status) {
          var HideHypoIcon = "display: none;";
          if (doc.data().Detergent == "Hypoallergenic") {
            HideHypoIcon = "";
          }
          var HideBonus = "style='display:none;'";
          if (doc.data().Bonus != 0) {
            HideBonus = "";
          }

          if (DistanceToSudster <= 48280) {
            OrdersArray.push({
              id: doc.id,
              Name: doc.data().CustomerFirstName,
              City: response.destinationAddresses.toString().split(", ")[1] + ", " + response.destinationAddresses.toString().split(",")[2].substring(1, 3),
              Distance: parseFloat((response.rows[0].elements[0].distance.value * 0.000621371192).toFixed(1)),
              Bags: NumberOfBags,
              HideBonus: HideBonus,
              Bonus: doc.data().Bonus,
              HideHypo: HideHypoIcon,
              Lat: doc.data().Lat,
              Lng: doc.data().Lng,
              TimeStamp: doc.data().TimeStamp
            });
          }

          Counter++;
          if (Counter >= snapshot.docs.length) {
            SetOrderPositions();
          }
        });



      });

      function SetOrderPositions() {

        if (NoOrders == false) {
          //Order order lists
          OrdersArray.sort(function (a, b) {
            return parseFloat(a.Distance) - parseFloat(b.Distance);
          });

          //Add Inside Orders to DOM
          for (var i = 0; i < OrdersArray.length; i++) {
            if ($("#GetOrderItem-" + OrdersArray[i].id).length == 0) {
              $("#GetOrdersHTMLDiv").append('<div id="GetOrderItem-' + OrdersArray[i].id + '" class="GetOrdersItem " onclick="AcceptOrder(' + i + ')"> <h1 class="GetOrdersItemName">' + OrdersArray[i].Name + '<small>' + OrdersArray[i].City + '</small></h1> <label class="GetOrderItemLabel"><span>' + OrdersArray[i].Distance + '<small>miles</small></span></label>  <label class="GetOrderItemLabel"><span>' + OrdersArray[i].Bags + '<small>bag(s)</small></span></label> <label ' + OrdersArray[i].HideBonus + ' class="GetOrderItemLabel GetOrderItemBonusLabel"><span>$' + OrdersArray[i].Bonus + '<small>Bonus</small></span></label> <i class="material-icons GetOrderItemHA" style="' + OrdersArray[i].HideHypo + '">&#xE23B;</i> <i class="material-icons GetOrderItemCheckmark ">arrow_forward</i> </div>');


              var MarkerLabel = (OrdersArray[i].Name).charAt(0).toUpperCase() + (OrdersArray[i].Name).slice(1);
              MarkerLabel = MarkerLabel.substring(0, 10);
              var MarkerIcon = "https://firebasestorage.googleapis.com/v0/b/laundry-llama.appspot.com/o/Assets%2FOrderMapIconBlue.png?alt=media&token=36120a86-d810-4d42-b3fa-e5f69a972d40";
              var MarkelLabelOrigin = new google.maps.Point(40, 12.5);
              if (OrdersArray[i].Bonus != 0) {
                MarkerIcon = "https://firebasestorage.googleapis.com/v0/b/laundry-llama.appspot.com/o/Assets%2FOrderMapBonusIconBlue.png?alt=media&token=7ec48dbb-17f8-42b0-8487-7ed6b027012b";
                MarkelLabelOrigin = new google.maps.Point(40, 28);
              }

              var marker = new google.maps.Marker({
                position: new google.maps.LatLng(OrdersArray[i].Lat, OrdersArray[i].Lng),
                map: OrdersMap,
                animation: google.maps.Animation.DROP,
                label: {
                  text: MarkerLabel,
                  color: "#FFFFFF",
                  fontSize: "14px",
                  fontFamily: "Work Sans"
                },
                title: '' + i,
                icon: {
                  labelOrigin: MarkelLabelOrigin,
                  url: MarkerIcon
                }
              });

              /*google.maps.event.addListener(marker, "click", function (e) {
                AcceptOrder(parseInt(marker.title));
              });*/

              (function (marker, i) {
                google.maps.event.addListener(marker, 'click', function () {
                  AcceptOrder(parseInt(marker.title));
                });
              })(marker, i);

            }
          }

        }

      }

      $("#GetOrdersLoading").css('display', "none");
      $("#HeaderExtraButton").removeClass('RotateIcon');
      $("#GetOrdersHTMLDiv").css("opacity", "1");
      if (NoOrders) {
        InitOrdersMap(WorkAreaCenter.latitude, WorkAreaCenter.longitude, WorkAreaRadius);
        $("#GetOrdersHTMLDiv").html('<span id="GetOrdersLoading">No Available Orders.<button onclick="PopupAlert(\'No Orders?\',\'Orders are assigned on a first-come first-serve basis. To increase your chances of getting orders, accept them immediately upon getting a New Order Notice.\')" id="NoOrdersPopupButton">Why not?</button></span>');
        //$("#GetOrdersHTMLDiv").append('<div id="GetOrderItem-" class="GetOrdersItem" onclick="OpenConfirmPopup()"> <h1 class="GetOrdersItemName">TESTING<small>BALTIMORE, MD</small></h1> <label class="GetOrderItemLabel"><span>10<small>miles</small></span></label>  <label class="GetOrderItemLabel"><span>2<small>bag(s)</small></span></label> <label class="GetOrderItemLabel GetOrderItemBonusLabel"><span>$5<small>Bonus</small></span></label> <i class="material-icons GetOrderItemHA" style="">&#xE23B;</i><i class="material-icons GetOrderItemCheckmark ">arrow_forward</i></div>');
      } else {
        InitOrdersMap(WorkAreaCenter.latitude, WorkAreaCenter.longitude, WorkAreaRadius);
      }

    });
  } else {
    InitOrdersMap(WorkAreaCenter.latitude, WorkAreaCenter.longitude, WorkAreaRadius);
    $("#GetOrdersHTMLDiv").html('<span id="GetOrdersLoading">No Available Orders.<button onclick="PopupAlert(\'No Orders?\',\'Orders are assigned on a first-come first-serve basis. To increase your chances of getting orders, accept them immediately upon getting a New Order Notice.\')" id="NoOrdersPopupButton">Why not?</button></span>');

  }
}

function AcceptOrder(OrderIndex, SkipConfirm) {
  var OrderData = OrdersArray[OrderIndex];
  var ID = OrderData.id;
  ToggleLoading();

  CFdatabase.collection("Orders/").where("SudsterID", "==", UserID).limit(25).get().then(function (SudsterOrdersDoc) {

    var ActiveOrderCount = 0;
    var OrderHistoryCount = 0;

    SudsterOrdersDoc.forEach(function (Order) {

      if (Order.data().Active) {
        ActiveOrderCount++;
      } else {
        OrderHistoryCount++;
      }

    });

    if (ActiveOrderCount == 1 && OrderHistoryCount == 0) {
      //only permited 1 order at a time for first order
      ToggleLoading();
      PopupAlert("Limit Reached", "As a new Sudster you must successfully complete 1 order and get a good rating before you can accept multiple orders at a time.")
    } else if (OrderHistoryCount <= 2 && ActiveOrderCount >= 3) {
      //max 5 orders at a time for new sudsters
      ToggleLoading();
      PopupAlert("Limit Reached", "As a new Sudster you must successfully complete 2 orders and get a good rating before you can accept more then 3 orders at a time.")
    } else if (ActiveOrderCount >= 20) {
      //max 10 orders at a time
      ToggleLoading();
      PopupAlert("Limit Reached", "You can only do a maximum of 20 orders at a time.")
    } else {
      if (SkipConfirm == true) {
        //GO
        var Sudster_AcceptOrder = firebase.functions().httpsCallable('Sudster_AcceptOrder');
        Sudster_AcceptOrder({
          UserID: UserID,
          ID: ID,
          PickupDeadlineUnix: CalcPickupDeadline(true),
          DeliveryDeadlineUnix: CalcDeliveryDeadline(OrderData.TimeStamp, new Date(), true),
        }).then(function (result) {

          ToggleLoading();
          GetOrderList();

          //Ask for pickup time
          OrderAcceptedPopup(result.data.CustomerName, ID)

        }).catch(function (err) {
          PopupAlert("Error", "There seems to be an error. (Someone might have already accepted this order.)");
          ToggleLoading();
          console.log(err);
        });

      } else {
        //Confirm order 
        ToggleLoading();
        ConfirmIndex = OrderIndex;

        $("#OC_MainInfoName").html(OrderData.Name + "<small>#" + ID + "</small>");
        $("#OC_BagCount").html(OrderData.Bags);
        $("#OC_City").html(OrderData.City);
        $("#OC_Miles").html(OrderData.Distance);
        if (OrderData.Bonus != 0) {
          $("#OC_Bonus").css("display", "block");
          $("#OC_Bonus span").html(OrderData.Bonus);
        }
        if (OrderHistoryCount != 0) {
          $('#OC_ConfirmSuppliesDiv').css("display", "none");
        }
        if (OrderData.HideHypo != "") {
          $('#OC_ConfirmHypoDiv').css("display", "none");
        } else {
          $('#OC_ConfirmHypoDiv').css("display", "block");
        }
        $("#OC_ConfirmPickupTime").html(CalcPickupDeadline());
        $("#OC_ConfirmDeliveryTime").html(CalcDeliveryDeadline(OrderData.TimeStamp, new Date()));

        OpenConfirmPopup();
      }
    }
  });

}

function OrderAcceptedPopup(Name, OrderNumber) {

  AcceptedOrderName = Name;
  AcceptedOrderNumber = OrderNumber;

  $("#PopupAcceptedPullDownDiv select").val("Choose");
  $("#PopupAcceptedPullDownDiv").css("display", "block");
  $("#PopupAcceptedMessageDiv").css("display", "none");
  $("#PopupAcceptedTextArea").val("");

  $("#PopupAccepted").show();
  $("#PopupAccepted").css("opacity", "1");
  $("#PopupAcceptedInnerDiv").css("margin-top", "-50px").css("opacity", "1");
  $("body").css("overflow", "hidden");
  $("#Screen").css("-webkit-filter", "blur(10px)").css("transform", "scale(1.05)");
  $("header").css("-webkit-filter", "blur(10px)");

  var CurrentHour = new Date().getHours();

  if (CurrentHour >= 17 && CurrentHour <= 20) {
    $("#PopupAcceptedOptionDiv").css("display", "block");
    $("#PopupAcceptedMessageDiv").css("display", "none");
    $("#PopupAcceptedTitleLabel").html("Select Pickup Window");
  } else {
    $("#PopupAcceptedTitleLabel").html("Introduce Yourself");
    $("#PopupAcceptedOptionDiv").css("display", "none");
    $("#PopupAcceptedMessageDiv").css("display", "block");
    $('#PopupAcceptedTextArea').val(GetRandomGreeting(false));
  }

}

function ClickAlertsCheckbox(element) {
  if (element.checked) {
    CFdatabase.doc("Sudsters/" + UserID).set({
      AlertsOn: true
    }, {
      merge: true
    });
  } else {
    CFdatabase.doc("Sudsters/" + UserID).set({
      AlertsOn: false
    }, {
      merge: true
    });
  }
}

function GetActiveOrderList() {
  CFdatabase.collection("Orders").where("SudsterID", "==", UserID).where("Active", "==", true).get().then(function (snapshot) {
    if (snapshot.empty) {
      $("#ActiveOrdersHTML").html('<span id="AOListLoading">No Active Orders.<button onclick="ClickMenu();ClickMenuItem(1, $(\'#MenuGetOrders\'));">Get More Orders</button></span>');
    } else {
      var ActiveOrderData = [];
      var ActiveOrderDataIDs = [];
      var WaypointArray = [];
      var WaypointOrder;
      var ActiveOrdersHTML = "";
      snapshot.forEach(function (doc) {
        ActiveOrderData.push(doc.data());
        ActiveOrderDataIDs.push(doc.id);
        WaypointArray.push({
          location: doc.data().Address
        });
      });
      var directionsService = new google.maps.DirectionsService;
      directionsService.route({
        origin: UsersAddress + " " + UsersCity + ", " + UsersState + " USA",
        destination: UsersAddress + " " + UsersCity + ", " + UsersState + " USA",
        waypoints: WaypointArray,
        optimizeWaypoints: true,
        travelMode: 'DRIVING'
      }, function (response, status) {
        if (status === 'OK') {
          WaypointOrder = response.routes[0].waypoint_order;
          MAODataArray = [];
          for (var i = 0; i < WaypointOrder.length; i++) {
            var AOListData = ActiveOrderData[WaypointOrder[i]];
            MAODataArray.push(ActiveOrderData[WaypointOrder[i]]);
            MAODataIDArray.push(ActiveOrderDataIDs[WaypointOrder[i]]);
            var AOListDataIDs = ActiveOrderDataIDs[WaypointOrder[i]];
            var BreadcrumbDoneClass = ["", "", "", ""];
            BreadcrumbDoneClass[AOListData.OrderStatusNumber - 1] = "BreadcrumbDone";
            if (AOListData.OrderStatusNumber >= 1) {
              BreadcrumbDoneClass[0] = "BreadcrumbDone";
            }
            if (AOListData.OrderStatusNumber >= 2) {
              BreadcrumbDoneClass[1] = "BreadcrumbDone";
            }
            if (AOListData.OrderStatusNumber >= 2.5) {
              BreadcrumbDoneClass[2] = "BreadcrumbDone";
            }
            if (AOListData.OrderStatusNumber >= 3) {
              BreadcrumbDoneClass[3] = "BreadcrumbDone";
            }
            var MsgHide = "style='display:none'";
            if (AOListData.SudsterMessageBadge != 0 && AOListData.SudsterMessageBadge != null) {
              MsgHide = "";
            }

            var DeadlineText = 'Pickup Deadline: <b>' + ConvertUnixToPickupTime(AOListData.PickupDeadline) + '</b>';
            if (AOListData.OrderStatusNumber >= 2) {
              DeadlineText = 'Delivery Deadline: <b>' + ConvertUnixToDeliveryDate(AOListData.DeliveryDeadline) + '</b>'
            }


            ActiveOrdersHTML += '<div onclick="ClickActiveOrder(\'' + AOListDataIDs + '\', \'' + i + '\')" class="AOListItemDiv"> <span id="AOListOrderNumber">#' + AOListDataIDs + '</span> <label ' + MsgHide + ' id="AOLMessageBadge"><i class="material-icons">&#xE0B7;</i>' + AOListData.SudsterMessageBadge + '</label> <h1 class="AOListName">' + AOListData.CustomerFirstName + '<small class="AOListDueDate">' + DeadlineText + '</small></h1> <div class="AOListBreadcrumbDiv"><span></span><i class="material-icons ' + BreadcrumbDoneClass[0] + '">&#xE0C8;</i><i class="material-icons ' + BreadcrumbDoneClass[1] + '">&#xE54A;</i><i class="material-icons ' + BreadcrumbDoneClass[2] + '">exposure</i><i class="material-icons ' + BreadcrumbDoneClass[3] + '">drive_eta</i></div> <i class="AOListArrow material-icons">&#xE5C8;</i>  <!--<i class="material-icons AOListAlertMsg">&#xE0B7;</i>--></div>';
          }
          $("#ActiveOrdersHTML").html(ActiveOrdersHTML);
        } else {
          console.log('Directions request failed due to ' + status);
        }
      });
    }
  });
}

function ClickActiveOrder(OrderID, Index) {

  MAOBagCounterActive = false;
  BagCounterNumber = 1;
  MAOIndex = Index;
  MAOID = OrderID;
  MAOPreAuth = MAODataArray[Index].AuthAmount;

  $("#LoadingDiv").css("display", "none").css("opacity", "0");
  $("#ManageAO").find(".MAOTaskItemCoverDiv").css("display", "block");
  Loading = false;

  MAONumber = {
    ID: OrderID,
    I: Index,
    N: MAODataArray[Index].CustomerFirstName
  };

  var Index = MAONumber.I;
  var OrderID = MAONumber.ID;

  GoToPage("ManageAO", "Manage Order", false, true, false, false);
  $("#MAOGoMsgButton").attr("onclick", "GoToMessages('" + OrderID + "');");
  $("#MAOMessageBadge").html(MAODataArray[Index].SudsterMessageBadge);
  $("#MAOMessageBadge").css("display", "block");
  if (MAODataArray[Index].SudsterMessageBadge == 0 || MAODataArray[Index].SudsterMessageBadge == null) {
    $("#MAOMessageBadge").css("display", "none");
  }
  $("#MAOName").html("<span>#" + MAODataIDArray[Index] + "</span>" + MAODataArray[Index].CustomerFirstName);

  var DeadlineText = 'Pickup Deadline: <b>' + ConvertUnixToPickupTime(MAODataArray[Index].PickupDeadline) + '</b>';
  if (MAODataArray[Index].OrderStatusNumber >= 2) {
    DeadlineText = 'Delivery Deadline: <b>' + ConvertUnixToDeliveryDate(MAODataArray[Index].DeliveryDeadline) + '</b>'
  }
  $("#MAODueText").html(DeadlineText);

  $("#MAOSpecialInstuctionsDiv").css("display", "none");


  if (MAODataArray[Index].OrderStatusNumber == 1 || MAODataArray[Index].OrderStatusNumber == 1.5) {
    //Going to Pickup Phase
    if (localStorage.getItem(MAOID) == null || parseInt(localStorage.getItem(MAOID)) < 1) {
      localStorage.setItem(MAOID, '1');
    }

    $("#MAO-BC-1").addClass("BreadcrumbProgress");
    $("#MAOTasksDiv1").css("display", "block").css("opacity", "1").css("left", "0px");
    var AddressLine2 = '';
    if (MAODataArray[Index].AddressLine2 != "") {
      AddressLine2 = ' of "' + MAODataArray[Index].AddressLine2 + '"';
    }
    $("#MAOPickupText").html(MAODataArray[Index].Address);
    $("#MAOLocateText").html("Locate " + MAODataArray[Index].OrderSize + " Bag(s) at \"" + MAODataArray[Index].ExactLocation + '"' + AddressLine2 + "");
    $("#MAOTaskPickupImg").attr('src', 'https://maps.googleapis.com/maps/api/streetview?size=400x150&location=' + MAODataArray[Index].Address + '&key=AIzaSyAbnx8mcEDXIG3_1Nt55xBlNm6IVllEXn8&fov=80&pitch=5');
    $("#MAOPickupAddressLink").attr("href", "https://www.google.com/maps/dir/?api=1&destination=" + MAODataArray[Index].Address);
    $("#MAOTaskCompleteButton1").attr("onclick", "OrderStepsCompleted(1, '" + MAODataArray[Index].UserID + "', '" + OrderID + "')");
    $("#MAOTaskLabeler1").html(MAODataArray[Index].CustomerFirstName + " - #" + MAODataIDArray[Index]);
  } else if (MAODataArray[Index].OrderStatusNumber == 2) {
    //Going to Wash Phase
    if (localStorage.getItem(MAOID) == null || parseInt(localStorage.getItem(MAOID)) < 8 || parseInt(localStorage.getItem(MAOID)) > 14) {
      localStorage.setItem(MAOID, '8');
    }

    $("#MAO-BC-1").addClass("BreadcrumbDone");
    $("#MAO-BC-2").addClass("BreadcrumbProgress");
    $("#MAOTasksDiv2").css("display", "block").css("opacity", "1");
    $(".MAOBagsNumText").html(localStorage.getItem(MAOID + '-CountPickupBags'));
    if (MAODataArray[Index].Detergent == "Premium Scented") {
      $("#MAODetergentType").html("Premium Scented");
    } else {
      $("#MAOHypoDiv").css("display", "block");
    }

    $("#MAOTaskLabeler2").html(MAODataArray[Index].CustomerFirstName + " - #" + MAODataIDArray[Index]);
    if (MAODataArray[MAOIndex].SpecificInstuction != "") {
      $("#MAOSpecialInstuctionsDiv").css("display", "block");
      $("#MAOSpecialInstuctionsDiv p").html('"' + MAODataArray[MAOIndex].SpecificInstuction + '"');
    }
    $("#MAOTaskCompleteButton2").attr("onclick", "OrderStepsCompleted(2, '" + MAODataArray[Index].UserID + "', '" + OrderID + "')");

    var Airdry = MAODataArray[MAOIndex].Airdry || true;
    if (Airdry) {
      $("#ShowAirDry").css("display", "inline-block");
    }

  } else if (MAODataArray[Index].OrderStatusNumber == 2.5) {
    //Going to Weigh Phase
    if (localStorage.getItem(MAOID) == null || parseInt(localStorage.getItem(MAOID)) < 15 || parseInt(localStorage.getItem(MAOID)) > 18) {
      localStorage.setItem(MAOID, '15');
    }

    $(".MAOBreadcrumbDiv i").removeClass("BreadcrumbProgress");
    $("#MAO-BC-1").addClass("BreadcrumbDone");
    $("#MAO-BC-2").addClass("BreadcrumbDone");
    $("#MAO-BC-3").addClass("BreadcrumbProgress");
    $("#MAOTasksDiv2").css("opacity", "0");
    $("#MAOTasksDiv1").css("display", "none");
    $("#MAOTasksDiv2").css("display", "none");
    $("#MAOTasksDiv3").css("display", "block").css("opacity", "1");
    $("#MAOTaskCompleteButton3").attr("onclick", "OrderStepsCompleted(3, '" + MAODataArray[Index].UserID + "', '" + OrderID + "')");

  } else if (MAODataArray[Index].OrderStatusNumber == 3) {
    //Going to Delivery Phase
    if (localStorage.getItem(MAOID) == null || parseInt(localStorage.getItem(MAOID)) < 19 || parseInt(localStorage.getItem(MAOID)) > 22) {
      localStorage.setItem(MAOID, '19');
    }

    $("#MAO-BC-1").addClass("BreadcrumbDone");
    $("#MAO-BC-2").addClass("BreadcrumbDone");
    $("#MAO-BC-3").addClass("BreadcrumbDone");
    $("#MAO-BC-4").addClass("BreadcrumbProgress");
    $("#MAOTasksDiv4").css("display", "block").css("opacity", "1");
    var AddressLine2 = '';
    if (MAODataArray[Index].AddressLine2 != "") {
      AddressLine2 = ' of "' + MAODataArray[Index].AddressLine2 + '"';
    }
    $("#MAODeliveryText").html(MAODataArray[Index].Address);
    $("#MAOTaskDeliveryImg").attr('src', 'https://maps.googleapis.com/maps/api/streetview?size=400x150&location=' + MAODataArray[Index].Address + '&key=AIzaSyAbnx8mcEDXIG3_1Nt55xBlNm6IVllEXn8&fov=80&pitch=5');
    $("#MAODeliveryAddressLink").attr("href", "https://www.google.com/maps/dir/?api=1&destination=" + MAODataArray[Index].Address);
    $("#MAODeliveryLocationText").html("Deliver " + MAODataArray[Index].OrderSize + " Bag(s) at \"" + MAODataArray[Index].ExactLocation + '"' + AddressLine2 + "" + ". If there are hung clothes, hang them on something.");
    $("#MAODeliveredButton").attr("onclick", "OrderStepsCompleted(3, '" + MAODataArray[Index].UserID + "', '" + MAODataIDArray[Index] + "')");
    $("#MAODeliveryCT1").html(MAODataArray[Index].TotalBagsPacked);
    $("#MAODeliveryCT2").html(MAODataArray[Index].FirstName);
    $("#MAODeliveryCT3").html(MAODataArray[Index].Address.split(",")[0] + " (" + MAODataArray[Index].ExactLocation + ")");
    $("#MAOTaskCompleteButton4").attr("onclick", "OrderStepsCompleted(4, '" + MAODataArray[Index].UserID + "', '" + MAODataIDArray[Index] + "')");
    if (MAODataArray[Index].HoldBackDelivery) {
      $("#HoldDeliveryDiv").css("display", "block");
      $("#HoldDeliveryBlurDiv").css("-webkit-filter", "blur(7px)");
    }
  }

}

function OrderStepsCompleted(TaskNumber, CustomerID, OrderNumber) {

  ToggleLoading();
  MAODataArray[MAOIndex].OrderStatusNumber = TaskNumber + 1;

  var OrderDate = new Date().toDateString();
  var OrderTime = formatAMPM(new Date());
  var BagWeightArray = JSON.parse(localStorage.getItem(MAOID + "-WeightArray"));
  var BagsTotalWeight = localStorage.getItem(MAOID + "-TotalWeight") || 0;
  var TotalBagsPickedup = $("#MAOTaskBagCountNumber").html() || "";
  var PickupPhoto = $("#MAOPickupPicture").attr("src") || "";
  var DeliveryPhoto = $("#MAODeliveryPicture").attr("src") || "";
  var TotalBagsDelivered = parseInt($("#MAOTaskDeliveryBagCountNumber").html()) || 0;

  if (TaskNumber == 3 && BagsTotalWeight == 0) {
    PopupAlert("No Bags Weighed", "Please add at least 1 bag to step 17.");
    ToggleLoading();
    return;
  }

  var Sudster_NextOrderStep = firebase.functions().httpsCallable('Sudster_NextOrderStep_V2');
  Sudster_NextOrderStep({
    OrderDate: OrderDate,
    OrderTime: OrderTime,
    TaskNumber: TaskNumber,
    CustomerID: CustomerID,
    OrderNumber: OrderNumber,
    BagWeightArray: BagWeightArray,
    BagsTotalWeight: BagsTotalWeight,
    TotalBagsPickedup: TotalBagsPickedup,
    UsersFirstName: UsersFirstName,
    TotalBagsDelivered: TotalBagsDelivered,
    PickupPhoto: PickupPhoto,
    DeliveryPhoto: DeliveryPhoto,
    DeliveryDate: ConvertUnixToDeliveryDate(MAODataArray[MAOIndex].DeliveryDeadlineUnix),
    DeliveryTime: MAODataArray[MAOIndex].DeliveryDeadlineUnix
  }).then(function (result) {

    $("#MAOHypoDiv").css("display", "none");
    $("#MAOSpecialInstuctionsDiv").css("display", "none");

    if (TaskNumber == 1) {
      //Going to Phase 2
      $(".MAOBreadcrumbDiv i").removeClass("BreadcrumbProgress");
      $("#MAO-BC-1").addClass("BreadcrumbDone");
      $("#MAO-BC-2").addClass("BreadcrumbProgress");
      $("#MAOTasksDiv1").css("opacity", "0");
      $("#MAOTaskCompleteButton2").attr("onclick", "OrderStepsCompleted(2, '" + CustomerID + "', '" + OrderNumber + "')");
      $(".MAOBagsNumText").html($("#MAOTaskBagCountNumber").html());
      $("#MAODueText").html('Delivery Deadline: <b>' + ConvertUnixToDeliveryDate(MAODataArray[MAOIndex].DeliveryDeadline) + '</b>');
      if (MAODataArray[MAOIndex].Detergent == "Premium Scented") {
        $("#MAODetergentType").html("Premium Scented");
      } else {
        $("#MAOHypoDiv").css("display", "block");
      }

      $("#MAOTaskLabeler2").html(MAODataArray[MAOIndex].CustomerFirstName + " - #" + MAODataIDArray[MAOIndex]);
      if (MAODataArray[MAOIndex].SpecificInstuction != "") {
        $("#MAOSpecialInstuctionsDiv").css("display", "block");
        $("#MAOSpecialInstuctionsDiv p").html('"' + MAODataArray[MAOIndex].SpecificInstuction + '"');
      }
      if (MAODataArray[MAOIndex].SpecificInstuction == "") {
        $("#MAOSpecialInstuctionsTitle").css("display", "none");
      } else {
        $("#MAOSpecialInstuctionsTitle").css("display", "block");
      }

      setTimeout(function () {
        $("#MAOTasksDiv1").css("display", "none");
        $("#MAOTasksDiv2").css("display", "block").css("opacity", "1");
        ToggleLoading();
        ClickMAOTaskItem($("#MAOTasksItem" + (localStorage.getItem(MAOID))));
        $("#Screen").animate({
          scrollTop: 0
        }, 00);

      }, 300);
    } else if (TaskNumber == 2) {
      //Going to Phase 3
      $(".MAOBreadcrumbDiv i").removeClass("BreadcrumbProgress");
      $("#MAO-BC-1").addClass("BreadcrumbDone");
      $("#MAO-BC-2").addClass("BreadcrumbDone");
      $("#MAO-BC-3").addClass("BreadcrumbProgress");
      $("#MAOTasksDiv2").css("opacity", "0");
      $("#MAOTasksDiv1").css("display", "none");
      $("#MAOTasksDiv2").css("display", "none");
      $("#MAOTasksDiv3").css("display", "block").css("opacity", "1");
      $("#MAOTaskCompleteButton3").attr("onclick", "OrderStepsCompleted(3, '" + CustomerID + "', '" + OrderNumber + "')");
      setTimeout(function () {
        ToggleLoading();
        ClickMAOTaskItem($("#MAOTasksItem" + (localStorage.getItem(MAOID))));
        $("#Screen").animate({
          scrollTop: 0
        }, 00);
      }, 300);
    } else if (TaskNumber == 3) {
      //Going to Phase 4 from Phase 3

      $(".MAOBreadcrumbDiv i").removeClass("BreadcrumbProgress");
      $("#MAO-BC-1").addClass("BreadcrumbDone");
      $("#MAO-BC-2").addClass("BreadcrumbDone");
      $("#MAO-BC-3").addClass("BreadcrumbDone");
      $("#MAO-BC-4").addClass("BreadcrumbProgress");
      $("#MAOTasksDiv3").css("opacity", "0");
      setTimeout(function () {
        $("#MAOTasksDiv3").css("display", "none");
        $("#MAOTasksDiv4").css("display", "block").css("opacity", "1");
        var AddressLine2 = '';
        if (MAODataArray[MAOIndex].AddressLine2 != "") {
          AddressLine2 = ' of "' + MAODataArray[MAOIndex].AddressLine2 + '"';
        }
        $("#MAODeliveryText").html(MAODataArray[MAOIndex].Address);
        $("#MAOTaskDeliveryImg").attr('src', 'https://maps.googleapis.com/maps/api/streetview?size=400x150&location=' + MAODataArray[MAOIndex].Address + '&key=AIzaSyAbnx8mcEDXIG3_1Nt55xBlNm6IVllEXn8&fov=80&pitch=5');
        $("#MAODeliveryAddressLink").attr("href", "https://www.google.com/maps/dir/?api=1&destination=" + MAODataArray[MAOIndex].Address);
        $("#MAODeliveryLocationText").html("Deliver " + MAODataArray[MAOIndex].OrderSize + " Bag(s) at \"" + MAODataArray[MAOIndex].ExactLocation + '"' + AddressLine2 + "" + ". If there are hung clothes, hang them on something.");
        $("#MAODeliveryCT1").html(MAODataArray[MAOIndex].TotalBagsPacked);
        $("#MAODeliveryCT2").html(MAODataArray[MAOIndex].FirstName);
        $("#MAODeliveryCT3").html(MAODataArray[MAOIndex].Address.split(",")[0] + " (" + MAODataArray[MAOIndex].ExactLocation + ")");
        $("#MAOTaskCompleteButton4").attr("onclick", "OrderStepsCompleted(4, '" + MAODataArray[MAOIndex].UserID + "', '" + MAODataIDArray[MAOIndex] + "')");
        if (MAODataArray[MAOIndex].HoldBackDelivery) {
          $("#HoldDeliveryDiv").css("display", "block");
          $("#HoldDeliveryBlurDiv").css("-webkit-filter", "blur(7px)");
        }
        ToggleLoading();
        ClickMAOTaskItem($("#MAOTasksItem" + (localStorage.getItem(MAOID))));
        $("#Screen").animate({
          scrollTop: 0
        }, 00);
      }, 300);

      var DeclinedOrder = MAODataArray[MAOIndex].OrderDeclined;
      if (DeclinedOrder != null && DeclinedOrder.OrderID == OrderNumber) {
        //Tell sudster not to deliver yet.
        $("#HoldDeliveryDiv").css("display", "block");
        $("#HoldDeliveryBlurDiv").css("-webkit-filter", "blur(7px)");
      }

    } else if (TaskNumber == 4) {
      //Going to Delete
      localStorage.removeItem(MAOID);
      localStorage.removeItem(MAOID + "-CountDeliveryBags");
      localStorage.removeItem(MAOID + "-DeliveryImage");
      localStorage.removeItem(MAOID + "-CountPickupBags");
      localStorage.removeItem(MAOID + "-YourWeight");
      localStorage.removeItem(MAOID + "-WeightArray");
      localStorage.removeItem(MAOID + "-TotalWeight");
      localStorage.removeItem(MAOID + "-PickupImage");
      localStorage.removeItem(MAOID + "-CountPickupBags");

      ToggleLoading(true);
      setTimeout(function () {
        GoBackPage();
      }, 1500);
    }

  }).catch(function (err) {

    ToggleLoading();
    PopupAlert("Error", err.message + "<br><br>(Please try again, or if the issue persists, please contact us.)");
    console.log(err);
  });

}

function formatAMPM(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'PM' : 'AM';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0' + minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
}

function convertToHHMMSS(s) {
  s = s * (1000 * 60 * 60);
  var ms = s % 1000;
  s = (s - ms) / 1000;
  var secs = s % 60;
  s = (s - secs) / 60;
  var mins = s % 60;
  var hrs = (s - mins) / 60;
  if (mins <= 9) {
    mins = "0" + mins;
  }
  if (secs <= 9) {
    secs = "0" + secs;
  }
  return hrs + ':' + mins + ':' + secs;
}

function convertToHHMM(s) {
  s = s * (1000 * 60 * 60);
  var ms = s % 1000;
  s = (s - ms) / 1000;
  var secs = s % 60;
  s = (s - secs) / 60;
  var mins = s % 60;
  var hrs = (s - mins) / 60;
  if (mins <= 9) {
    mins = "0" + mins;
  }
  if (secs <= 9) {
    secs = "0" + secs;
  }
  return hrs + ':' + mins;
}

function MAOMessages() {}

function AddMAOBagWeightLine(PrependNumber) {
  if (PrependNumber == null) {
    PrependNumber = "000.0";
  }
  if (BagCounterNumber < 50) {

    var EmptyFeildsCount = 0;
    var i = 0;
    for (i = 0; i < $(".MAOWeighListItem").length; i++) {
      if ($(".MAOWeighListItem:eq(" + (i - 1) + ") input").val() == "") {
        EmptyFeildsCount++;
      }
    }

    if (EmptyFeildsCount < 1) {

      BagCounterNumber++;

      $("#MAOWeighListDivHTML").append('<div class="MAOWeighListItem">' +
        '<label>YOU + BAG</label>' +
        '<input onclick="MAOWeightOnClick(this)" oninput="MAOWeightOnInput(this)" onblur="MAOWeightItemsInputBlur(this)" id="MAOWeighListInput1" value="' + PrependNumber + '" type="number" inputmode="numeric" pattern="[0-9]*">' +
        '<i class="material-icons" onclick="MAOWeightItemRemove(this)">cancel</i>' +
        '<span>0.0 lbs</span>' +
        '<div class="MAOWeighListItemOverlayDiv">' +
        'Label Bag with Weight: <b>0.0 LBS</b>' +
        '</div>' +
        '</div>');

      $("#MAOTasksItem17").css("max-height", $("#MAOTasksItem17").find(".MAOTaskItemInner").height() + 50);

    } else {

      ShakeElement(".MAOWeighListItem:eq(" + (i - 1) + ")");
    }

  }
}

function MAOWeightItemsInputBlur(Element) {


  if ($(Element).val() != '') {
    var TotalWeightNum = parseFloat($(Element).val());
    if (Number.isInteger(TotalWeightNum) && $(Element).val().split(".")[1] != "0") {
      $(Element).val('');
      $(Element).blur();
      PopupAlert("Enter Exact Weight", "Please enter the exact weight to the decimal (e.g " + TotalWeightNum + ".4, " + TotalWeightNum + ".0, ect). It is very important that you weigh accuratly, and entering your weight even 1 pound off can get your account suspended.")
    } else {
      var BagWeightNum = (TotalWeightNum - parseFloat(localStorage.getItem(MAOID + "-YourWeight"))).toFixed(1);

      if (BagWeightNum > 15) {
        $(Element).val('000.0');
        PopupAlert('Too Heavy', 'Each bag must be under 15 pounds. Please remove ' + parseFloat(BagWeightNum - 15).toFixed(1) + ' lbs from this bag.');
      } else if (BagWeightNum < 1 && TotalWeightNum > 0) {
        $(Element).val('000.0');
        PopupAlert("Something's Wrong", "You were supposed to enter your weight while holding a bag of laundry. But the weight you entered was less than your body weight. Please try again. Enter your weight while holding a bag. ")
      } else if (TotalWeightNum > 0) {
        //Good to go
        $(Element).parent().find("span").html("<b>" + BagWeightNum + "</b> LBS")
        $(Element).val(TotalWeightNum.toFixed(1));
        $(Element).parent().addClass("OverlayAnimation");
        $(Element).parent().find(".MAOWeighListItemOverlayDiv b").html(BagWeightNum + " LBS");
        setTimeout(function () {
          $(Element).parent().removeClass("OverlayAnimation");
        }, 5000);
      }
      MAOBagCounterActive = true;
    }
  }
  setTimeout(() => {
    SaveMAOWeighItems();
  }, 100);

}

function MAOWeightInputBlur(Input) {

  var InputNum = parseFloat($(Input).val());
  localStorage.removeItem(MAOID + "-YourWeight");

  if (InputNum < 60 || InputNum > 700 || $(Input).val() == "") {
    PopupAlert("Enter YOUR weight", "Weigh yourself (without a bag) and enter your weight.");
    localStorage.removeItem(MAOID + "-YourWeight");
    localStorage.setItem(MAOID, "16");
    SetMAOStepFromMemery();
  } else {

    //Good to go
    localStorage.setItem(MAOID + "-YourWeight", InputNum.toFixed(1) + "");

    if (localStorage.getItem(MAOID + "-WeightArray") != null) {
      localStorage.removeItem(MAOID + "-WeightArray");
      localStorage.removeItem(MAOID + "-TotalWeight");
      for (var i = 0; i < $(".MAOWeighListItem").length; i++) {
        MAOWeightItemRemove($(".MAOWeighListItem:eq(" + (i - 1) + ") input"))
      }
      LoadMAOWeighItems();
    }


  }

}

function MAOWeightOnInput(Input) {
  var OriganalValue = $(Input).val();
  var WithoutDecimal = parseInt(OriganalValue.replace(".", ""));
  var ReverseString = pad(parseFloat(WithoutDecimal * 0.1).toFixed(1), 5).toString().split("").reverse().join("").substring(0, 5);
  $(Input).val(ReverseString.split("").reverse().join(""));
}

function MAOWeightOnClick(Input) {
  var tmpStr = $(Input).val();
  $(Input).val('');
  $(Input).val(tmpStr).focus().val(tmpStr);
}

function pad(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function SaveMAOWeighItems() {
  var TotalWeight = 0;
  var YourWeight = parseFloat(localStorage.getItem(MAOID + "-YourWeight"));
  var BagWeightArray = [];
  localStorage.setItem(MAOID, "17");
  for (var i = 0; i < $(".MAOWeighListItem").length; i++) {

    if ($(".MAOWeighListItem:eq(" + (i) + ") input").val() != "000.0") {
      BagWeightArray.push(($(".MAOWeighListItem:eq(" + (i) + ") input").val() - YourWeight).toFixed(1));
      TotalWeight = parseFloat(TotalWeight + parseFloat($(".MAOWeighListItem:eq(" + (i) + ") input").val()) - YourWeight);
    }
  }

  localStorage.setItem(MAOID + "-WeightArray", JSON.stringify(BagWeightArray));
  localStorage.setItem(MAOID + "-TotalWeight", TotalWeight.toFixed(1) + "");

  if (TotalWeight == 0.0 || TotalWeight == "NaN") {
    localStorage.removeItem(MAOID + "-WeightArray");
    localStorage.removeItem(MAOID + "-TotalWeight");
  }

}

function LoadMAOWeighItems() {
  var YourWeight = parseFloat(localStorage.getItem(MAOID + "-YourWeight"));
  var WeightArray = JSON.parse(localStorage.getItem(MAOID + "-WeightArray"));
  if (WeightArray != null) {
    if ($(".MAOWeighListItem").length < WeightArray.length) {
      for (var i = 0; i < WeightArray.length; i++) {
        AddMAOBagWeightLine((parseFloat(WeightArray[i]) + YourWeight).toFixed(1));
        var BagWeightNum = parseFloat(WeightArray[i]).toFixed(1);
        $(".MAOWeighListItem:eq(" + (i) + ")").find("span").html("<b>" + BagWeightNum + "</b> LBS")
      }
    }
  } else {
    if ($(".MAOWeighListItem").length < 1) {
      AddMAOBagWeightLine();
    }
  }

}


function MAOWeightItemRemove(Element) {
  $(Element).parent().remove();
  BagCounterNumber--;
  SaveMAOWeighItems();
}

function TurnPNOn() {
  CFdatabase.doc("Sudsters/" + UserID).set({
    AlertsOn: true
  }, {
    merge: true
  }).then(function (docRef) {
    if (Platform == "IOS") {
      try {
        window.webkit.messageHandlers.Sudsters.postMessage({
          task: "AskForPN",
          userid: UserID
        });
      } catch (e) {}
    } else {}
    AllowedPN = true;
    ContinueSignUp(6);
  });
}

function deleteCollection(db, collectionRef, batchSize) {
  var query = collectionRef.limit(batchSize);
  return new Promise(function (resolve, reject) {
    deleteQueryBatch(db, query, batchSize, resolve, reject);
  });
}

function deleteQueryBatch(db, query, batchSize, resolve, reject) {
  query.get().then((snapshot) => {
    // When there are no documents left, we are done
    if (snapshot.size == 0) {
      return 0;
    }
    // Delete documents in a batch
    var batch = db.batch();
    snapshot.docs.forEach(function (doc) {
      batch.delete(doc.ref);
    });
    return batch.commit().then(function () {
      return snapshot.size;
    });
  }).then(function (numDeleted) {
    if (numDeleted <= batchSize) {
      resolve();
      return;
    }
    // Recurse on the next process tick, to avoid
    // exploding the stack.
    process.nextTick(function () {
      deleteQueryBatch(db, query, batchSize, resolve, reject);
    });
  }).catch(reject);
}

function LoadOrderHistoryItems() {

  CFdatabase.collection("Orders").where("SudsterID", "==", UserID).where("Active", "==", false).orderBy("TimeStamp", "desc").get().then(function (collection) {

    $("#OH-HTMLDiv").html("");
    collection.forEach(function (doc) {

      var RatingIconCode = "";
      var RatingIconColor = "";
      if (doc.data().Rating.Number == 1) {
        RatingIconCode = "&#xE814";
        RatingIconColor = "#d62d20";
      } else if (doc.data().Rating.Number == 3) {
        RatingIconCode = "&#xE813";
        RatingIconColor = "#ffa700";
      } else if (doc.data().Rating.Number == 5) {
        RatingIconCode = "&#xE815";
        RatingIconColor = "#008744";
      }

      $("#OH-HTMLDiv").append('<div class="OH-Item"> <label>' + doc.data().ViewTime + '</label> <h1>' + doc.data().CustomerFirstName + '<small>#' + doc.id + '</small></h1> <div class="OH-Item-MoneyDiv"> $' + (Math.floor(doc.data().TotalEarned / 100)) + '<sup>.' + doc.data().TotalEarned.toString().slice(-2) + '</sup> </div> <h6><i class="material-icons" style="color: ' + RatingIconColor + ';">' + RatingIconCode + '</i> "' + doc.data().Rating.Note + '"</h6> </div> ');

    });


    if (collection.docs.length == 0) {
      $("#OH-HTMLDiv").html('<span id="AOListLoading">No Order History Yet.</span>');
    }

  });

}

function LoadMoneyTab() {

  var BalanceCents = TotalBalance.toString().slice(-2);
  if (BalanceCents.length == 1) {
    BalanceCents = BalanceCents + "0";
  }

  if (TotalBalance < 0) {
    TotalBalance = 0;
    BalanceCents = "00";
  }
  $("#MoneyBalence").html("<sup>&nbsp;&nbsp;$</sup>" + (Math.floor(TotalBalance / 100)) + "<sup>." + BalanceCents + "</sup>");

  PayoutFee = ((TotalBalance / 100) * 0.05).toFixed(2);

  CFdatabase.collection("Orders").where("SudsterID", "==", UserID).where("Active", "==", false).orderBy("TimeStamp", "desc").limit(50).get().then(function (BalanceHistory) {

    $("#MoneyListDiv").html("");
    BalanceHistory.forEach(function (doc) {

      var MoneyCents = doc.data().TotalEarned.toString().slice(-2);
      if (MoneyCents.length == 1) {
        MoneyCents = MoneyCents += "0";
      }

      var MoneyDate = doc.data().TimeStamp;
      var MonthArray = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
      var DateMonth = MonthArray[MoneyDate.getMonth()];

      var AmountCents = doc.data().TotalEarned.toString().slice(-2);

      if (AmountCents.length == 1) {
        AmountCents = AmountCents + "0";
      }

      var HideBonus = "";
      var LowerAmountHeight = "";
      var ExtraMoneyAmount = "";
      if (doc.data().Bonus == 0 && doc.data().TipProfit == 0) {
        //No bonus or tip
        HideBonus = "style='display:none'";
        LowerAmountHeight = "style='padding-top:10px;font-size:20px'";
      } else if (doc.data().Bonus == 0 && doc.data().TipProfit != 0) {
        //Just a tip
        ExtraMoneyAmount = (doc.data().TipProfit / 100).toFixed(2) + " Tip";
      } else if (doc.data().Bonus != 0 && doc.data().TipProfit != 0) {
        //Bonus and Tip
        ExtraMoneyAmount = ((doc.data().Bonus) + (Math.floor(doc.data().TipProfit / 100))).toFixed(2) + " Tip/Bonus";
      } else if (doc.data().Bonus != 0 && doc.data().TipProfit == 0) {
        //Just a bonus
        ExtraMoneyAmount = doc.data().Bonus + " Bonus";
      }

      $("#MoneyListDiv").append('<div class="MoneyListItem">' +
        '<h1><small>' + DateMonth + '</small>' + MoneyDate.getDate() + '</h1>' +
        '<h2>' + doc.data().CustomerFirstName + '<small>#' + doc.id + '</small>' + '</h2>' +
        '<h3 ' + LowerAmountHeight + '>$' + (Math.floor(doc.data().TotalEarned / 100)) + '<sup>.' + AmountCents + '</sup><label ' + HideBonus + '>$' + ExtraMoneyAmount + '</label></h3>' +
        '</div>');


    });

  });


}

function ClickPayoutButton() {
  if (AllowInstantPayout) {
    PopupHTML('<h1 id="PayoutTitle">Instant Payout</h1><h2 id="PayoutSubtitle">Get paid within minutes! | 3.9% Fee</h2><button id="PayoutButton" onclick="SendPayout()">Get Paid Now</button>', true);
  } else {
    HowPayoutsWork();
  }
}

function SendPayout() {

  ClosePopupHTML();

  if (AllowInstantPayout) {
    ToggleLoading();

    var Sudster_InstantPayout = firebase.functions().httpsCallable('Sudster_InstantPayout');
    Sudster_InstantPayout({
      StripeID: StripeAccountID
    }).then(function (result) {


      CFdatabase.doc("Sudsters/" + UserID).update({
        Balance: 0,
        PendingBalance: 0
      }).then(function (docRef) {
        AllowInstantPayout = false;
        LoadMoneyTab();
        ToggleLoading(true);
      });


    }).catch(function (err) {
      PopupAlert("Error", err.message);
      ToggleLoading();
      console.log(err);
    });

  }

}

function ScheduleSlideUp(ElementNum) {

  var RandomExpTime = (86400000 * (Math.floor(Math.random() * 14) + 7));

  if (ElementNum == 2) {
    if (localStorage.getItem("SlideUp-" + ElementNum) == null) {
      localStorage.setItem("SlideUp-" + ElementNum, new Date().getTime() + "");
    }
    if (new Date().getTime() >= parseInt(localStorage.getItem("SlideUp-" + ElementNum))) {
      var ExpTime = (new Date().getTime() + RandomExpTime);
      localStorage.setItem("SlideUp-" + ElementNum, ExpTime + "");
      $("#TipSlideUpDiv h1").html("Don’t Knock or Ring");
      $("#TipSlideUpDiv div").html("When you pick up or deliver, do not knock or ring the doorbell (unless the customer specifically requested). The customer is instructed to put the laundry outside at the pick-up location and expects not to be disturbed during pickup or delivery.");
      ToggleTipSlideup();
    }
    /*else {
         if (localStorage.getItem("SlideUp-" + ElementNum + ".1") == null) {
           localStorage.setItem("SlideUp-" + ElementNum + ".1", new Date().getTime() + "");
         }
         if (new Date().getTime() >= parseInt(localStorage.getItem("SlideUp-" + ElementNum + ".1"))) {
           var ExpTime = (new Date().getTime() + 604800000);
           localStorage.setItem("SlideUp-" + ElementNum + ".1", ExpTime + "");
           $("#TipSlideUpDiv h1").html("test");
           $("#TipSlideUpDiv div").html("testing");
           ToggleTipSlideup();
         }
       }*/

  } else if (ElementNum == 4) {
    if (localStorage.getItem("SlideUp-" + ElementNum) == null) {
      localStorage.setItem("SlideUp-" + ElementNum, new Date().getTime() + "");
    }
    if (new Date().getTime() >= parseInt(localStorage.getItem("SlideUp-" + ElementNum))) {
      var ExpTime = (new Date().getTime() + RandomExpTime);
      localStorage.setItem("SlideUp-" + ElementNum, ExpTime + "");
      $("#TipSlideUpDiv h1").html("Never Take Hampers");
      $("#TipSlideUpDiv div").html("If a customer puts their clothes out in a hamper, transfer the clothes to a plastic bag, label it, and leave the hamper behind.");
      ToggleTipSlideup();
    }
  } else if (ElementNum == 10) {
    if (localStorage.getItem("SlideUp-" + ElementNum) == null) {
      localStorage.setItem("SlideUp-" + ElementNum, new Date().getTime() + "");
    }
    if (new Date().getTime() >= parseInt(localStorage.getItem("SlideUp-" + ElementNum))) {
      var ExpTime = (new Date().getTime() + RandomExpTime);
      localStorage.setItem("SlideUp-" + ElementNum, ExpTime + "");
      $("#TipSlideUpDiv h1").html("Never Deliver Wet Laundry");
      $("#TipSlideUpDiv div").html("SudShare is a wash-<b><u>dry</u></b>-fold service. Laundry must be delivered completely dry. If you deliver wet or damp laundry you will NOT be paid for the job. Make sure all items are completely dry.");
      ToggleTipSlideup();
    }
  } else if (ElementNum == 11) {
    if (localStorage.getItem("SlideUp-" + ElementNum) == null) {
      localStorage.setItem("SlideUp-" + ElementNum, new Date().getTime() + "");
    }
    if (localStorage.getItem("SlideUp-" + ElementNum + ".1") == null) {
      localStorage.setItem("SlideUp-" + ElementNum + ".1", new Date().getTime() + "");
    }
    if (localStorage.getItem("SlideUp-" + ElementNum + ".2") == null) {
      localStorage.setItem("SlideUp-" + ElementNum + ".2", new Date().getTime() + "");
    }
    if (localStorage.getItem("SlideUp-" + ElementNum + ".3") == null) {
      localStorage.setItem("SlideUp-" + ElementNum + ".3", new Date().getTime() + "");
    }
    if (localStorage.getItem("SlideUp-" + ElementNum + ".4") == null) {
      localStorage.setItem("SlideUp-" + ElementNum + ".4", new Date().getTime() + "");
    }

    if (new Date().getTime() >= parseInt(localStorage.getItem("SlideUp-" + ElementNum))) {
      var ExpTime = (new Date().getTime() + RandomExpTime);
      localStorage.setItem("SlideUp-" + ElementNum, ExpTime + "");
      $("#TipSlideUpDiv h1").html("Turn Clothes Right Side Out");
      $("#TipSlideUpDiv div").html("Don't forget to turn all clothes right side out before folding them. Customers don't like to receive clothes inside out. All clothes should be delivered ready-to-wear.");
      ToggleTipSlideup();
    } else if (new Date().getTime() >= parseInt(localStorage.getItem("SlideUp-" + ElementNum + ".1"))) {
      if (new Date().getTime() >= parseInt(localStorage.getItem("SlideUp-" + ElementNum + ".1"))) {
        var ExpTime = (new Date().getTime() + RandomExpTime);
        localStorage.setItem("SlideUp-" + ElementNum + ".1", ExpTime + "");
        $("#TipSlideUpDiv h1").html("Make Different Piles for each Family Member");
        $("#TipSlideUpDiv div").html("When you're folding, make different piles for each family member so that the laundry is easy for customers to put away. Within each family member's pile, put like-items together. In other words, stack shirts with shirts, pants with pants, etc. ");
        ToggleTipSlideup();
      }
    } else if (new Date().getTime() >= parseInt(localStorage.getItem("SlideUp-" + ElementNum + ".2"))) {
      if (new Date().getTime() >= parseInt(localStorage.getItem("SlideUp-" + ElementNum + ".2"))) {
        var ExpTime = (new Date().getTime() + RandomExpTime);
        localStorage.setItem("SlideUp-" + ElementNum + ".2", ExpTime + "");
        $("#TipSlideUpDiv h1").html("Button & Zip");
        $("#TipSlideUpDiv div").html("When you're folding, be careful to button and zip everything neatly. Do not leave garments unbuttoned or unzipped.");
        ToggleTipSlideup();
      }
    } else if (new Date().getTime() >= parseInt(localStorage.getItem("SlideUp-" + ElementNum + ".3"))) {
      if (new Date().getTime() >= parseInt(localStorage.getItem("SlideUp-" + ElementNum + ".3"))) {
        var ExpTime = (new Date().getTime() + RandomExpTime);
        localStorage.setItem("SlideUp-" + ElementNum + ".3", ExpTime + "");
        $("#TipSlideUpDiv h1").html("Match Socks");
        $("#TipSlideUpDiv div").html("When you're folding, be sure to match socks and deliver them in pairs.  All socks should be delivered ready-to-wear.");
        ToggleTipSlideup();
      }
    } else if (new Date().getTime() >= parseInt(localStorage.getItem("SlideUp-" + ElementNum + ".4"))) {
      if (new Date().getTime() >= parseInt(localStorage.getItem("SlideUp-" + ElementNum + ".4"))) {
        var ExpTime = (new Date().getTime() + RandomExpTime);
        localStorage.setItem("SlideUp-" + ElementNum + ".4", ExpTime + "");
        $("#TipSlideUpDiv h1").html("All Clothes Must Be Right Side Out");
        $("#TipSlideUpDiv div").html("Before you fold a garment, make sure it's right side out. All clothes must be delivered ready-to-wear. Never return clothes inside out.");
        ToggleTipSlideup();
      }
    }

  } else if (ElementNum == 12) {
    if (localStorage.getItem("SlideUp-" + ElementNum) == null) {
      localStorage.setItem("SlideUp-" + ElementNum, new Date().getTime() + "");
    }
    if (localStorage.getItem("SlideUp-" + ElementNum + ".1") == null) {
      localStorage.setItem("SlideUp-" + ElementNum + ".1", new Date().getTime() + "");
    }
    if (localStorage.getItem("SlideUp-" + ElementNum + ".2") == null) {
      localStorage.setItem("SlideUp-" + ElementNum + ".2", new Date().getTime() + "");
    }

    if (new Date().getTime() >= parseInt(localStorage.getItem("SlideUp-" + ElementNum))) {
      var ExpTime = (new Date().getTime() + RandomExpTime);
      localStorage.setItem("SlideUp-" + ElementNum, ExpTime + "");
      $("#TipSlideUpDiv h1").html("Let’s Be Clear");
      $("#TipSlideUpDiv div").html("Some Sudsters ask: “Do my delivery bags have to be clear?”<br><br>The answer is YES.<br><br>Your delivery bags must be CLEAR and COLORLESS.");
      ToggleTipSlideup();
    } else if (new Date().getTime() >= parseInt(localStorage.getItem("SlideUp-" + ElementNum + ".1"))) {
      if (new Date().getTime() >= parseInt(localStorage.getItem("SlideUp-" + ElementNum + ".1"))) {
        var ExpTime = (new Date().getTime() + RandomExpTime);
        localStorage.setItem("SlideUp-" + ElementNum + ".1", ExpTime + "");
        $("#TipSlideUpDiv h1").html("Are Drawstring Bags Permitted?");
        $("#TipSlideUpDiv div").html("No! Do not use drawstring bags. They do not completely seal. They leave about a 1 inch opening at the top where water and/or dirt could get in. Instead, use a large bag with lots of excess at the top and use the excess to tie a water resistant knot.");
        ToggleTipSlideup();
      }
    } else {
      if (new Date().getTime() >= parseInt(localStorage.getItem("SlideUp-" + ElementNum + ".2"))) {
        var ExpTime = (new Date().getTime() + RandomExpTime);
        localStorage.setItem("SlideUp-" + ElementNum + ".2", ExpTime + "");
        $("#TipSlideUpDiv h1").html("Bags in does NOT have to Equal Bags Out");
        $("#TipSlideUpDiv div").html("Some Sudsters mistakenly think that if a customer gives you 1 bag of laundry that you have to give them back 1 bag of laundry. Not true! You can split a customer’s laundry up into as many bags as you want. Customers will often stuff bags full of dirty laundry, making them big and burdensome. But you want to make sure to return to them smaller, lighter, easier to handle bags, even if that means that you give them back 4x as many bags as they gave you.");
        ToggleTipSlideup();
      }
    }

  } else if (ElementNum == 15) {
    if (localStorage.getItem("SlideUp-" + ElementNum) == null) {
      localStorage.setItem("SlideUp-" + ElementNum, new Date().getTime() + "");
    }
    if (new Date().getTime() >= parseInt(localStorage.getItem("SlideUp-" + ElementNum))) {
      var ExpTime = (new Date().getTime() + (RandomExpTime * 10000));
      localStorage.setItem("SlideUp-" + ElementNum, ExpTime + "");
      $("#TipSlideUpDiv h1").html("How We Weigh");
      $("#TipSlideUpDiv div").html("First you'll weigh yourself (without a bag) and enter your weight into the app. Then you'll weigh yourself while holding each bag and enter those weights in the app. The app will do the math to calculate the weight of the laundry.");
      ToggleTipSlideup();
    }
  } else if (ElementNum == 17) {
    if (localStorage.getItem("SlideUp-" + ElementNum) == null) {
      localStorage.setItem("SlideUp-" + ElementNum, new Date().getTime() + "");
    }
    if (new Date().getTime() >= parseInt(localStorage.getItem("SlideUp-" + ElementNum))) {
      var ExpTime = (new Date().getTime() + (RandomExpTime));
      localStorage.setItem("SlideUp-" + ElementNum, ExpTime + "");
      $("#TipSlideUpDiv h1").html("Knot Tie Delivery Bags");
      $("#TipSlideUpDiv div").html("Knot tie the top of each bag so it’s sealed and water resistant from rain. If your delivery bags are not sealed, rain and/or dirt may get in and ruin the clean laundry. If this happens your customer will be refunded and you will not be paid for the job.");
      ToggleTipSlideup();
    }
  } else if (ElementNum == 19) {
    if (localStorage.getItem("SlideUp-" + ElementNum) == null) {
      localStorage.setItem("SlideUp-" + ElementNum, new Date().getTime() + "");
    }
    if (new Date().getTime() >= parseInt(localStorage.getItem("SlideUp-" + ElementNum))) {
      var ExpTime = (new Date().getTime() + (RandomExpTime));
      localStorage.setItem("SlideUp-" + ElementNum, ExpTime + "");
      $("#TipSlideUpDiv h1").html("Hang for Delivery");
      $("#TipSlideUpDiv div").html("Customers request certain clothes be hung for multiple reasons, one of which is that they don’t want the clothes to wrinkle. When you deliver hang-dry clothes HANG THEM on something so they don’t get wrinkled.");
      ToggleTipSlideup();
    }
  } else if (ElementNum == "Messaging") {
    if (localStorage.getItem("SlideUp-" + ElementNum) == null) {
      localStorage.setItem("SlideUp-" + ElementNum, new Date().getTime() + "");
    }
    if (new Date().getTime() >= parseInt(localStorage.getItem("SlideUp-" + ElementNum))) {
      var ExpTime = (new Date().getTime() + (RandomExpTime));
      localStorage.setItem("SlideUp-" + ElementNum, ExpTime + "");
      $("#TipSlideUpDiv h1").html("Respond to All Messages");
      $("#TipSlideUpDiv div").html("Don't forget...if a customer messages you, you must respond. And you must respond within 1 hour. <br><br> Even if there’s nothing important to say, even if all the practical information has been communicated, you should always send the last message. You can say something like 'Thank you', 'Got it', 'Have a nice day', 'OK', ect. <br><br> Again, always respond to a customer message, and always respond within 1 hour.");
      ToggleTipSlideup();
    }
  }


}

function MAOStepCompleteClick(Element, ElementNum, PrePress) {


  var OK = true;


  if (PrePress != true) {
    if (ElementNum == 4 && $("#MAOPickupPicture").attr("src") == "") {
      OK = false;
      ShakeElement($("#MAOPickupPicture").parent());
    }
    if (ElementNum == 6 && $("#MAOTaskBagCountNumber").html() == "0") {
      OK = false;
      ShakeElement($("#MAOTaskBagCounter"));
    }
    if (ElementNum == 16 && localStorage.getItem(MAOID + "-YourWeight") == null) {
      OK = false;
      ShakeElement($("#MAOYourWeightInput"));
    }
    if (ElementNum == 17 && localStorage.getItem(MAOID + "-WeightArray") == null) {
      OK = false;
      ShakeElement("#MAOWeighListDiv");
    }
    if (ElementNum == 21 && $("#MAOTaskDeliveryBagCountNumber").html() == "0") {
      ShakeElement($("#MAOTaskDeliveryBagCountNumber").parent());
      OK = false;
    }
    if (ElementNum == 22 && $("#MAODeliveryPicture").attr("src") == "") {
      ShakeElement($("#MAODeliveryPicture").parent());
      OK = false;
    }
    if (ElementNum == 21) {
      if (localStorage.getItem(MAOID + "-WeightArray")) {
        var TotalBagsDelivered = parseInt($("#MAOTaskDeliveryBagCountNumber").html()) || 0;
        var PackedBagsLength = JSON.parse(localStorage.getItem(MAOID + "-WeightArray")).length || 0;
        if (TotalBagsDelivered != PackedBagsLength && PackedBagsLength != 0) {
          PopupAlert("Missing Bags", "The amount of bags you delivered does not match the amount of bags you packed. Please correct your error.");
          OK = false;
        }
      }
    }
  }

  if (OK) {

    if (PrePress != true) {
      var y = $("#Screen").scrollTop();
      $("#Screen").animate({
        scrollTop: (y + 200)
      }, 200);
      if (localStorage.getItem(MAOID) == null) {
        if (ElementNum < 8) {
          localStorage.setItem(MAOID, '1');
        } else if (ElementNum < 15) {
          localStorage.setItem(MAOID, '8');
        } else {
          localStorage.setItem(MAOID, '15');
        }
      } else {
        if (ElementNum >= parseInt(localStorage.getItem(MAOID))) {
          localStorage.setItem(MAOID, (ElementNum + 1).toString());
        }
      }
    }

    ScheduleSlideUp(ElementNum);

    if (ElementNum == 17 && (localStorage.getItem(MAOID + "-TotalWeight") * 100) > MAOPreAuth) {
      PopupAlert("Be Careful", "The weight of the laundry you’re reporting is substantially different than our system predicted for this customer’s order.<br><br>Take caution to report the weight accurately. Customers will verify what you report and if you overcharge them we will lose a customer, your account will be closed, the customer will be refunded, and you will not be paid for the job.")
    }
    if (ElementNum == 16) {
      LoadMAOWeighItems();
    }


    if ($("#MAOTaskBagCountNumber").html() != "0" && $("#MAOTaskBagCountNumber").html() != MAODataArray[MAOIndex].OrderSize && ElementNum == 6 && PrePress != true) {
      PopupAlert("Missing Bags", "The number of bags you picked up does not match the number of bags the customer put out. Please pick up " + MAODataArray[MAOIndex].OrderSize + " bag(s) or message your customer about the discrepancy.")
    }

    if (ElementNum >= 8 || parseInt(localStorage.getItem(MAOID)) >= 8) {
      $("#MAOTaskCompleteButton1").attr("disabled", false);
    } else {
      $("#MAOTaskCompleteButton1").attr("disabled", true);
    }

    if (ElementNum >= 15 || parseInt(localStorage.getItem(MAOID)) >= 15) {
      $("#MAOTaskCompleteButton2").attr("disabled", false);
    } else {
      $("#MAOTaskCompleteButton2").attr("disabled", true);
    }

    if (ElementNum >= 19 || parseInt(localStorage.getItem(MAOID)) >= 19) {
      $("#MAOTaskCompleteButton3").attr("disabled", false);
    } else {
      $("#MAOTaskCompleteButton3").attr("disabled", true);
    }
    if (ElementNum >= 23 || parseInt(localStorage.getItem(MAOID)) >= 23) {
      $("#MAOTaskCompleteButton4").attr("disabled", false);
    } else {
      $("#MAOTaskCompleteButton4").attr("disabled", true);
    }

    if (PrePress != true) {
      $(Element).removeClass("PulseAnimate").css("color", "#FFAB00").html("check_box");
      $("#MAOTasksItem" + (ElementNum + 1)).removeClass("Locked");
      if ($("#MAOTasksItem" + (ElementNum + 1) + " .StatusIcon").html() != "check_box") {
        $("#MAOTasksItem" + (ElementNum + 1) + " .StatusIcon").html("check_box_outline_blank");
      }
      setTimeout(() => {
        ClickMAOTaskItem($("#MAOTasksItem" + (ElementNum + 1)));
      }, 1);

    }

  }

}

function ToggleTipSlideup() {

  if ($("#TipSlideUpDiv").hasClass("TipSlideUpOpen")) {
    $("#TipSlideUpParentDiv").animate({
      opacity: "0"
    });
    $('#TipSlideUpDiv').addClass('TipSlideUpClose');
    setTimeout(function () {
      $('#TipSlideUpDiv').removeClass('TipSlideUpClose').removeClass('TipSlideUpOpen');
      $("#TipSlideUpParentDiv").css("display", "none");
    }, 400);
  } else {
    setTimeout(function () {
      $("#TipSlideUpParentDiv").animate({
        opacity: "1"
      });
      $("#TipSlideUpDiv").addClass("TipSlideUpOpen");
      $("#TipSlideUpParentDiv").css("display", "block");
    }, 1500);
  }

}

function SetMAOStepFromMemery() {

  var Step = parseInt(localStorage.getItem(MAOID));
  var StepLength = $(".MAOTasksItem").length;


  if (Step >= 4 && localStorage.getItem(MAOID + "-PickupImage") != null) {
    ShowPickupPicture(localStorage.getItem(MAOID + "-PickupImage"));
  }
  if (Step >= 6 && localStorage.getItem(MAOID + '-CountPickupBags') != null) {
    $("#MAOTaskBagCountNumber").html(localStorage.getItem(MAOID + '-CountPickupBags'));
  }

  if (Step >= 8) {
    $("#MAOTaskCompleteButton1").attr("disabled", false);
  }
  if (Step >= 15) {
    $("#MAOTaskCompleteButton2").attr("disabled", false);
  }
  if (Step >= 19) {
    $("#MAOTaskCompleteButton3").attr("disabled", false);
  }
  if (Step >= 23) {
    $("#MAOTaskCompleteButton4").attr("disabled", false);
  }

  if (Step >= 16) {
    $("#MAOYourWeightInput").val(localStorage.getItem(MAOID + "-YourWeight") || "000.0");
  }
  if (Step >= 17) {
    LoadMAOWeighItems();
  }
  if (Step >= 21 && localStorage.getItem(MAOID + '-CountDeliveryBags') != null) {
    $("#MAOTaskDeliveryBagCountNumber").html(localStorage.getItem(MAOID + '-CountDeliveryBags'));
  }
  if (Step >= 22 && localStorage.getItem(MAOID + "-DeliveryImage") != null) {
    ShowDeliveryPicture(localStorage.getItem(MAOID + "-DeliveryImage"));
  }

  for (var i = 1; i < StepLength; i++) {
    if (i < Step) {
      //Completed
      $("#MAOTasksItem" + (i) + " .StatusIcon").removeClass("PulseAnimate").css("color", "#FFAB00").html("check_box");
      $("#MAOTasksItem" + i).removeClass("Locked");

    } else if (i == Step) {
      $("#MAOTasksItem" + i + " .StatusIcon").html("check_box_outline_blank");
      $("#MAOTasksItem" + i).removeClass("Locked").addClass("PulseAnimate");
    } else {

      //Locked
      $("#MAOTasksItem" + i).addClass("Locked");
      $("#MAOTasksItem" + (i) + " .StatusIcon").html("indeterminate_check_box");

    }

  }

  ClickMAOTaskItem(("#MAOTasksItem" + Step));

}

function ClickMAOTaskItem(Element) {
  if ($(Element).hasClass("Locked") == false) {
    if ($(Element).hasClass("Active") == false || $(Element).css("max-height") == "50px") {
      $(".MAOTasksItem").css("max-height", 50).removeClass("Active");
      $(Element).css("max-height", $(Element).find(".MAOTaskItemInner").height() + 50).addClass("Active");
      $(Element).find(".StatusIcon").css("pointer-events", "all");
    }
  } else {
    ShakeElement($(".MAOTasksItem.Active .MAOTaskItemHeaderDiv"));
  }
}

function OnlyNumberPress(evt) {
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode(key);
  var regex = /[0-9]|\./;
  if (!regex.test(key)) {
    theEvent.returnValue = false;
    if (theEvent.preventDefault) theEvent.preventDefault();
  }
}

function ClickResourcesTab(Element, Num) {

  $(".TabItem").removeClass("Selected");
  $(Element).addClass("Selected");

  if (Num == 1) {
    $("#SuppliesInnerDiv").css("display", "block");
    $("#ImproveInnerDiv").css("display", "none");
  } else if (Num == 2) {
    $("#SuppliesInnerDiv").css("display", "none");
    $("#ImproveInnerDiv").css("display", "block");
  }

}

function ImproveItemClick(Number) {

  var title = "";
  var body = "";

  switch (Number) {

    case 0:
      title = "Basic Training Video";
      body = '<div id="SignupTrainingVideoDiv" class="valign"><video id="SignupTrainingVideo" style="width:100%;height: 100%;"><source src="https://firebasestorage.googleapis.com/v0/b/laundry-llama.appspot.com/o/Trainingvideo.mp4?alt=media&token=18067798-643a-4876-9903-dca43a4a02b4"></video><i id="SignupTrainingPlayButton" onclick="$(\'#SignupTrainingVideo\').trigger(\'play\');$(this).css(\'display\',\'none\');$(\'#SignupTrainingVideo\').attr(\'controls\', \'\');StartTrainingVideoCountdown();" class="material-icons">&#xE038;</i></div>';
      break;

    case 1:
      title = "List of Approved Detergents";
      body = "<p>We recommend and customers expect that you use one of the below listed laundry detergents. They don&rsquo;t cost any more, and according to Consumer Reports about 60 points separate the top picks from the worst performers. Bottom line: the detergent you use matters. Here are Consumer Reports top picks for laundry detergents&hellip;</p> <ul> <li>TidePlus Ultra Stain Release</li> <li>Persil ProClean Power-Liquid 2-in-1 laundry detergent</li> <li>PersilProClean Power-Liquid</li> <li>Green Works Laundry Detergent</li> <li>Wisk Deep Clean</li> <li>Persil Pro Clean Power-Liquid Sensitive Skin</li> <li>Member's MarkUltimate Clean</li> <li>PersilProClean Power-Pearls</li> <li>Tide Plus Coldwater Clean HE</li> <li>Kirkland Signature Ultra Clean</li> </ul> <p>If you haven&rsquo;t already, please make sure you&rsquo;re using one of these detergents. Your customers and your family will appreciate it.</p><br> <p>Nachshon Fertel</p> <p>President</p> <p>SudShare, Inc.</p>"
      break;
    case 2:
      title = "What You Need to Know on Day 1";
      body = '<p>Here&rsquo;s a brief list of some important matters to be clear on before you begin.</p> <ol> <li><strong>Next Day Delivery Guaranteed. </strong>Customers are guaranteed Next Day delivery, so don&rsquo;t accept orders you can&rsquo;t complete on time.</li> </ol> <ol> <li><strong>Never Wash Colors &amp; Whites Together. </strong>Colors and whites should always be separate loads.</li> </ol> <ol> <li><strong>Keep Air-Dry Clothes Separate. </strong>Customers will sometimes request that certain items be air-dry. You can keep these items separate either by doing a separate load or by putting the air-dry clothes in a <a href="https://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dhpc&amp;field-keywords=zippered+wash+bags">mesh bag</a> before putting them in the wash with machine-dry clothes.</li> </ol> <ol> <li><strong>Required Supplies &amp; Equipment. </strong>Here&rsquo;s what you&rsquo;ll need to get started:</li> </ol> <ul> <li>A washer &amp; dryer</li> <li>A drying rack, clothesline, or something to air-dry clothes</li> <li>Laundry Detergent (See list of approved detergents in &ldquo;Resources&rdquo; section the app)</li> <li>Laundry Baskets</li> <li>A Pen</li> <li><a href="https://www.amazon.com/Plasticplace-Clear-Trash-Bags-Gallon/dp/B0017T70B0/ref=sr_1_2_s_it?s=hpc&amp;ie=UTF8&amp;qid=1509723178&amp;sr=1-2&amp;keywords=clear+trash+bags+30+gallon&amp;refinements=p_n_feature_keywords_four_browse-bin%3A7932361011"><em>Clear</em> bags to pack folded laundry. These bags must be clear.</a></li> <li><a href="https://www.amazon.com/Generic-Adhesive-Labels-Sheets-LABELS/dp/B01N6KJ4HS/ref=sr_1_1?ie=UTF8&amp;qid=1518040415&amp;sr=8-1&amp;keywords=600+white+labels+generic+white+self+adhesive+labels">Labels for tracking laundry bags</a></li> <li><a href="https://www.amazon.com/Etekcity-Digital-Bathroom-Technology-Included/dp/B00F3J9G1W/ref=sr_1_cc_1_a_it?s=aps&amp;ie=UTF8&amp;qid=1518040497&amp;sr=1-1-catcorr&amp;keywords=etekcity+digital+weight">Scale to weigh clothes</a></li> <li>A car or some way to pick-up and deliver</li> </ul> <p>Other than the above, the app is pretty intuitive and self-explanatory, but if you have any questions feel free to be in touch.</p> <br><p>Sincerely,<br> Nachshon Fertel<br> President<br> SudShare, Inc.</p><br><br>'
      break;
    case 3:
      title = "5 Ways to Save Time &amp; Money Doing Laundry";
      body = '<p>As a Sudster with SudShare, you run your own business, so you want to save time and money when you can. Here are 5 simple ways to do laundry in less time and for less money.</p> <ol> <li><strong>Quick Wash Cycle. </strong>Some washing machines have a &ldquo;quick wash cycle&rdquo; that doesn&rsquo;t compromise performance. It takes half the time of a normal cycle, saves on water and energy, and gets the clothes just as clean. If your machine has this feature consider using it.</li> <li><strong>Wash in Cold Water.</strong> Consumer Reports recommends <em>not</em> washing clothes in hot water. Washers have improved and so have detergents. You can save energy washing in cold water. And it gets the job done just the same. In fact, detergents today are less effective in hot water. Washing in hot water also runs the risk that clothes shrink, fade, or get damaged. In general, hot water is hard on fabrics and reduces their lifespan. Washing in cold water reduces the likelihood of shrinking, fading, bleeding, and damage. Bottom line: always wash in cold water. It saves energy, extends the life of clothes, and gets clothes cleaner.</li> <li><strong>Wash Air-Dry and Machine-Dry Together.</strong> Air-dry and machine-dry clothes obviously have to be kept separate, but you can save time by putting the air-dry clothes in a netted laundry bag and washing them together with the same-color machine-dry clothes. You just saved yourself a load.</li> <li><strong>Use a Fan and Dehumidifier. </strong>Are your air-dry clothes holding you up? 2 suggestions: a) Put a fan on them. They&rsquo;ll dry in less than half the time. b) Put a dehumidifier in the room. The clothes will dry quicker.</li><li><strong>Service Multiple Customers. </strong>Pick-up and delivery takes time, but if you&rsquo;re doing it for multiple customers it takes less time per customer so you end up making more money per hour. Get the most out of your pick-up and delivery time by servicing multiple customers.</li> </ol> <p>There you have it&mdash;5 simple ways to make more money in less time.</p><br> <p>Sincerely,</p> <p>Nachshon Fertel</p> <p>President</p> <p>SudShare, Inc.</p><br><br>';
      break;
    case 4:
      title = "How to get More Customers";
      body = '<p>As you know, every Sudster has a ranking. The higher your rank, the more customers you get. Your rank is largely determined by your customer reviews. Here are 3 ways to get &ldquo;off the chart&rdquo; reviews from your customers.</p> <ol> <li><strong>SAME DAY DELIVERY. </strong>Next Day turnaround is mandatory, but customers love to get their laundry back same-day. Want to get a great review? Provide same-day turnaround.</li> <li><strong>PACK BY FAMILY MEMBER. </strong>Normally, when customers get their laundry back they have to separate items according to each member of the family. When you&rsquo;re folding and packing, you can&rsquo;t know which items belong to who, but you can guess and pack the laundry so it&rsquo;s more convenient for the customer. Want to really impress your customers? Try to pack the clothes according to each family member.</li> <li><strong>FOLD TO THE &ldquo;T&rdquo;.</strong> Don&rsquo;t you just love the way high-end fashion stores fold clothes? Want to know how they make the clothes look like that? They use a folding board, like Flip Fold. And if you want great reviews, so should you. Also, learn how to fold socks, fitted sheets, bras, and other tough to fold items like a pro. Just Google it and watch a short &ldquo;how to&rdquo; video on folding tough items.</li> </ol> <p>There you have it&mdash;3 simple ways to get great reviews, improve your rank, and thus get more customers.&nbsp;</p> <p>Warm regards,<br><br> Nachshon Fertel<br> President<br> SudShare, Inc.</p>';
      break;
    case 5:
      title = '4 Important Reminders';
      body = '<p>As a Sudster, you represent yourself, all the other Sudsters, and the entire SudShare network. It&rsquo;s important to take pride your work, conduct yourself professionally, and be careful to adhere to all SudShare terms and conditions.</p> <p>Here are 4 important reminders:</p> <ol> <li>Only communicate with customers through the app. Never call, text, or contact the customer directly.</li> <li>For your safety, never give customers your last name, phone number, or any personal or contact information.</li> <li>Always deliver on time.</li> <li>Always present yourself professionally and conduct yourself in a dignified manner.</li> </ol> <p>Thank you so much for being a Sudster.<br /><br /> Nachshon Fertel<br /> President<br /> SudShare, Inc</p>';
      break;
    case 6:
      title = 'Let’s be Clear—These Bags are Required';
      body = '<p>Some Sudsters ask: &ldquo;Do my delivery bags have to be clear?&rdquo;</p><br> <p>The answer is YES.</p><br> <p>You may <em>not</em> use colored trash bags (including white) to deliver laundry to customers. It&rsquo;s an ugly presentation and not fitting for the SudShare brand. In addition, customers don&rsquo;t always get the same Sudster, and consistently of presentation is important.</p><br> <p>Bottom line: you must use CLEAR trash bags for delivering laundry. Sudsters not in compliance will be removed from the network.</p><br> <p>Here&rsquo;s a list of some brands along with links to where you can buy clear bags online:</p><br> <p><a href="https://www.costco.com/Kirkland-Signature-33-Gallon-Trash-Bag-200-pack.product.100224134.html">Kirkland Signature 33 Gallon Trash Bag 200-pack </a>($.13/bag)</p><br> <p><a href="https://www.walmart.com/ip/Classic-Clear-Clear-Low-Density-Trash-Bags-31-33gal-63-Mil-33-x-39-Clear-250-Carton/14983470">Classic Clear Clear Low-Density Trash Bags, 31-33gal, 250/Carton</a> ($.21/bag)</p><br> <p><a href="https://www.amazon.com/Plasticplace-Clear-Trash-Bags-Gallon/dp/B0017T70B0/ref=sr_1_2_s_it?s=hpc&amp;ie=UTF8&amp;qid=1509723178&amp;sr=1-2&amp;keywords=clear+trash+bags+30+gallon&amp;refinements=p_n_feature_keywords_four_browse-bin%3A7932361011">Plasticplace Clear Trash Bags, 33 Gallon, 100/case</a> ($.25/bag)</p><br> <p><a href="https://www.amazon.com/Cand-Gallon-Clear-Large-Counts/dp/B01G1L9KJS/ref=sr_1_4_s_it?s=hpc&amp;ie=UTF8&amp;qid=1509723178&amp;sr=1-4&amp;keywords=clear+trash+bags+30+gallon&amp;refinements=p_n_feature_keywords_four_browse-bin%3A7932361011">Cand 30 Gallon Clear Large Trash Bags, 70 Counts</a> ($.26/bag)</p><br> <p><a href="https://www.amazon.com/Glad-Clear-Recycling-Drawstring-Gallon/dp/B00A9T31VW/ref=sr_1_1_a_it?ie=UTF8&amp;qid=1509722672&amp;sr=8-1&amp;keywords=Glad+Clear+Recycling+Drawstring+Large+Trash+Bags%2C+30+Gallon">Glad Clear Recycling Drawstring Large Trash Bags, 30 Gallon</a> ($.33/bag)</p><br> <p><a href="https://www.walmart.com/ip/Presto-Clear-Trash-30-Gallon-Flap-Top-Bags-20-count/15561336">Presto Clear Trash 30 Gallon Flap Top Bags, 20 count</a> ($.12/bag)</p><br> <p>Thank you.<br /><br /> Nachshon Fertel<br /> President<br /> SudShare, Inc.</p>';
      break;
    case 7:
      title = 'Best Practices';
      body = '<p>I want to remind you of a few important steps in the laundry process.</p><br> <p><strong><em>1 at a Time</em></strong></p> <p>When you&rsquo;re ready to begin washing, make sure to work with 1 customer&rsquo;s clothes at a time. This is very important. If you mix customers clothes you won&rsquo;t be allowed in the SudShare network.</p><br> <p><strong><em>Separate</em></strong></p> <p>The first step in the washing process is to separate the customer&rsquo;s clothes into the following bins, each of which should be a separate load:</p> <ul> <li>Lights &ndash; Machine Dry</li> <li>Colors &ndash; Machine Dry</li> <li>Lights &ndash; Air Dry</li> <li>Colors &ndash; Air Dry</li> <li>Towels &ndash; Machine Dry</li> </ul> <p><em>Never wash light and darks together or mix towels with the other items. </em></p><br> <p><strong><em>Label</em></strong></p> <p>Label each bin, not only to keep track of the customer, but so you know which clothes are machine-dry and which are air-dry. Make sure the label follows the clothes throughout the process, all the way to the delivery bag.</p><br> <p><strong><em>Prepare Clothes</em></strong></p> <p>Prepare the clothes for washing. That means 3 things:</p> <ul> <li>Close zippers so they don&rsquo;t catch on other clothes</li> <li>Turn jeans inside out to prevent streaky lines</li> <li>Turn clothes right side out</li> </ul> <p><strong><em>Washing</em></strong></p> <ul> <li>Don&rsquo;t overstuff the washing machine, or the clothes won&rsquo;t wash as well.</li> <li>Use one of the Sudshare recommended detergents. They don&rsquo;t cost any more, and according to Consumer Reports about 60 points separate the top picks from the worst performers. Bottom line: the detergent you use matters.</li> <li>Follow dosage instructions. In addition to being a waste of money, using too much detergent can leave residue in the clothes. Also, oversudsing is not good for your washing machine.</li> </ul> <p><strong><em>Folding</em></strong></p> <ul> <li>Take the clothes out of the dryer as soon as the cycle is compete, otherwise clothes wrinkle.</li> <li>Match socks</li> <li>Button and zip everything</li> <li>Fold very neatly</li> </ul> <p><strong><em>Packing</em></strong></p> <p>Make sure each delivery bag is labeled.</p> <p>Hope that helps. Take care!</p><br> Nachshon Fertel<br> President<br> <p>SudShare, Inc.</p>';
      break;
    case 8:
      title = "How to Properly Prepare Clothes for Washing";
      body = '<p>3 simple tips for preparing clothes for washing.</p> <ol> <li>Close zippers so they don&rsquo;t catch on other clothes</li> <li>Turn jeans inside out to prevent streaky lines</li> <li>Turn clothes right side out</li> </ol> <p>Your customers will appreciate you following through on these tips. And you know what that means? Higher customer ratings! And higher ratings means you get more business.</p><br> <p>Nachshon Fertel<br> President<br> SudShare, Inc.</p>';
      break;
    case 9:
      title = "Before Your Next Load...";
      body = '<p>Before you put your next load in the wash, think about this:</p> <ol> <li>Don&rsquo;t overstuff the washer, or the clothes won&rsquo;t wash as well.</li> <li>Follow dosage instructions. In addition to being a waste of money, using too much detergent can leave residue in the clothes. Also, oversudsing is not good for your washing machine.</li> <li>Use a cold water cycle. It gets the clothes just as clean and it&rsquo;ll save on your energy bill.</li> </ol> <p>Nachshon Fertel<br> President<br> SudShare, Inc.</p>';
      break;
  }

  $("#ArticleTitle").html(title);
  $("#ArticleBody").html(body);

  GoToPage("Article", "Improve", false, true);

}

function GetRandomGreeting(Tomorrow) {

  var Time = "in the next few hours"
  if (Tomorrow) {
    Time = "between 8-11am tomorrow";
  }
  if (new Date().getHours() > 20 || new Date().getHours() < 8) {
    Time = "between 8-11am";
  }
  var GreetingArray = ["Hey " + AcceptedOrderName + "! I'm " + UsersFirstName + ", your Sudster. I'll be there " + Time + " to pick up your laundry. Please make sure it’s out for me. Thank you :)",
    "Hey there " + AcceptedOrderName + "! My name is " + UsersFirstName + ". I'll be picking up your laundry " + Time + ". Please make sure it’s out for me. Thank you :)",
    "Hi " + AcceptedOrderName + ". My name is " + UsersFirstName + " and I'll be your Sudster for this order. I will be there sometime " + Time + " to pick up your laundry. Please make sure it’s in your pickup location. Let me know if you have any questions.",
    "Hi " + AcceptedOrderName + "! My name is " + UsersFirstName + " and I'll be your Sudster. I will be there " + Time + ". Please make sure your laundry is outside for me. Thanks! :)",
    "Hello " + AcceptedOrderName + ". I'm your Sudster, " + UsersFirstName + ". I'll swing by to get your laundry " + Time + ". Please make sure it’s in your pickup location. Thank you.",
    "Hi. I'm " + UsersFirstName + ", your Sudster. I'll be there " + Time + " to pickup your laundry. Please have it out for me. Thank you " + AcceptedOrderName + ".",
    "Hi " + AcceptedOrderName + "! I'm " + UsersFirstName + ", your Sudster. I'll swing by " + Time + " to pick up your laundry. Please have it out for me :) Thank you."
  ]

  return GreetingArray[Math.floor(Math.random() * GreetingArray.length)];

}

function SendGreetingMessage(Text) {

  if (Text != "") {

    ToggleLoading();

    var messageObject = {}
    messageObject[new Date().getTime()] = {
      Text: Text,
      Name: UsersFirstName,
      Time: formatAMPM(new Date()),
      Customer: false
    };



      //Central Order Hub START
      var DaysOfWeek = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"]
      CFdatabase.doc('Orders/' + AcceptedOrderNumber + "/Messages/" + new Date().getTime()).set({
        Text: Text,
        Name: UsersFirstName,
        Time: DaysOfWeek[new Date().getDay()] + " - " + formatAMPM(new Date())
      }).then(function () {

      }).catch(function (error) {
        console.log("COH Error -", error);
      });

      //Central Order Hub END


      ToggleLoading(true);
      $("body").css("overflow-y", "scroll")
      $("#PopupAccepted").css("opacity", "0");
      $("#PopupAcceptedInnerDiv").css("margin-top", "200px");
      $("#Screen").css("-webkit-filter", "blur(0px)").css("transform", "none");;
      $("header").css("-webkit-filter", "blur(0px)");
      setTimeout(function () {
        $("#PopupAccepted").hide(0);
      }, 500);



  }

}

$(window).blur(function () {

});
$(window).focus(function () {
  if (CurrentPage == "GetOrders") {
    GetOrderList();
  }

});
if (!google.maps.Polygon.prototype.getBounds) {
  google.maps.Polygon.prototype.getBounds = function (latLng) {
    var bounds = new google.maps.LatLngBounds(),
      paths = this.getPaths(),
      path,
      p, i;
    for (p = 0; p < paths.getLength(); p++) {
      path = paths.getAt(p);
      for (i = 0; i < path.getLength(); i++) {
        bounds.extend(path.getAt(i));
      }
    }
    return bounds;
  };
}
google.maps.Polygon.prototype.containsLatLng = function (latLng) {
  // Exclude points outside of bounds as there is no way they are in the poly
  var inPoly = false,
    bounds, lat, lng,
    numPaths, p, path, numPoints,
    i, j, vertex1, vertex2;
  // Arguments are a pair of lat, lng variables
  if (arguments.length == 2) {
    if (typeof arguments[0] == "number" && typeof arguments[1] == "number") {
      lat = arguments[0];
      lng = arguments[1];
    }
  } else if (arguments.length == 1) {
    bounds = this.getBounds();
    if (!bounds && !bounds.contains(latLng)) {
      return false;
    }
    lat = latLng.lat();
    lng = latLng.lng();
  } else {
    console.log("Wrong number of inputs in google.maps.Polygon.prototype.contains.LatLng");
  }
  // Raycast point in polygon method
  numPaths = this.getPaths().getLength();
  for (p = 0; p < numPaths; p++) {
    path = this.getPaths().getAt(p);
    numPoints = path.getLength();
    j = numPoints - 1;
    for (i = 0; i < numPoints; i++) {
      vertex1 = path.getAt(i);
      vertex2 = path.getAt(j);
      if (vertex1.lng() < lng && vertex2.lng() >= lng || vertex2.lng() < lng && vertex1.lng() >= lng) {
        if (vertex1.lat() + (lng - vertex1.lng()) / (vertex2.lng() - vertex1.lng()) * (vertex2.lat() - vertex1.lat()) < lat) {
          inPoly = !inPoly;
        }
      }
      j = i;
    }
  }
  return inPoly;
};

function getDistance(lat1, lon1, lat2, lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2 - lat1); // deg2rad below
  var dLon = deg2rad(lon2 - lon1);
  var a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
    Math.sin(dLon / 2) * Math.sin(dLon / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c; // Distance in km
  var dm = d * 0.621371; //Distance in Mi
  return dm;
}

function deg2rad(deg) {
  return deg * (Math.PI / 180)
}

function resetOrientation(file, srcBase64, callback) {
  getOrientation(file, function (srcOrientation) {
    console.log(srcOrientation);
    var img = new Image();
    img.onload = function () {
      var width = img.width,
        height = img.height,
        canvas = document.createElement('canvas'),
        ctx = canvas.getContext("2d");
      // set proper canvas dimensions before transform & export
      if (4 < srcOrientation && srcOrientation < 9) {
        canvas.width = height;
        canvas.height = width;
      } else {
        canvas.width = width;
        canvas.height = height;
      }
      // transform context before drawing image
      switch (srcOrientation) {
        case 2:
          ctx.transform(-1, 0, 0, 1, width, 0);
          break;
        case 3:
          ctx.transform(-1, 0, 0, -1, width, height);
          break;
        case 4:
          ctx.transform(1, 0, 0, -1, 0, height);
          break;
        case 5:
          ctx.transform(0, 1, 1, 0, 0, 0);
          break;
        case 6:
          ctx.transform(0, 1, -1, 0, height, 0);
          break;
        case 7:
          ctx.transform(0, -1, -1, 0, height, width);
          break;
        case 8:
          ctx.transform(0, -1, 1, 0, 0, width);
          break;
        default:
          break;
      }
      // draw image
      ctx.drawImage(img, 0, 0);
      // export base64
      callback(canvas.toDataURL());
    };
    img.src = srcBase64;
  });

  function getOrientation(file, callback) {
    var reader = new FileReader();
    reader.onload = function (e) {
      var view = new DataView(e.target.result);
      if (view.getUint16(0, false) != 0xFFD8) {
        return callback(-2);
      }
      var length = view.byteLength,
        offset = 2;
      while (offset < length) {
        if (view.getUint16(offset + 2, false) <= 8) return callback(-1);
        var marker = view.getUint16(offset, false);
        offset += 2;
        if (marker == 0xFFE1) {
          if (view.getUint32(offset += 2, false) != 0x45786966) {
            return callback(-1);
          }
          var little = view.getUint16(offset += 6, false) == 0x4949;
          offset += view.getUint32(offset + 4, little);
          var tags = view.getUint16(offset, little);
          offset += 2;
          for (var i = 0; i < tags; i++) {
            if (view.getUint16(offset + (i * 12), little) == 0x0112) {
              return callback(view.getUint16(offset + (i * 12) + 8, little));
            }
          }
        } else if ((marker & 0xFF00) != 0xFF00) {
          break;
        } else {
          offset += view.getUint16(offset, false);
        }
      }
      return callback(-1);
    };
    reader.readAsArrayBuffer(file);
  }
}

var SignupTestRight = 0;
var SignupTestNumber = 0;
var SignupTestArray = [{
    Q: 'Laundry must be delivered to the customer in what color bags?',
    A1: 'White',
    A2: 'Black',
    A3: 'Clear',
    CA: "Clear",
    ET: 'Use Clear Bags Only',
    E: 'You may not use colored trash bags (including white) to deliver laundry to customers. It’s an ugly presentation and not fitting for the SudShare brand. In addition, customers don’t always get the same Sudster, and consistently of presentation is important. Bottom line: you must use CLEAR trash bags for delivering laundry. Sudsters not in compliance will be removed from the network.'
  },
  {
    Q: 'What’s the deadline to deliver laundry back to the customer the next day?',
    A1: '6PM',
    A2: '8PM',
    A3: '9PM',
    CA: "8PM",
    ET: 'Delivery Deadline',
    E: '8PM the Next Day'
  },
  {
    Q: 'If you accept an order at 2PM, you must pick up the customer’s laundry by what time?',
    A1: '5PM',
    A2: '6PM',
    A3: '8PM',
    CA: "5PM",
    ET: 'Pickup Within 3 Hours',
    E: 'After you accept an order, you must pick up the customer’s laundry within 3 hours, unless you accepted the order after 5PM, in which case you can pick up before 8PM or between 8-11AM the next day.'
  },
  {
    Q: 'When is the permitted pickup and delivery window?',
    A1: '9AM-5PM',
    A2: '8AM-8PM',
    A3: '6AM-6PM',
    CA: "8AM-8PM",
    ET: 'Delivery Window',
    E: '8AM-8PM'
  },
  {
    Q: 'Can you cancel an order after you accept it?',
    A1: 'Yes',
    A2: 'No',
    A3: 'Sometimes',
    CA: "No",
    ET: 'Do NOT Cancel Orders',
    E: 'Once you accept an order, you CANNOT cancel it. Cancelling an order after you accept will result in your account being closed.'
  },
  {
    Q: 'What will happen to your account If you cancel an order after you accept it?',
    A1: 'Closed',
    A2: 'Paused',
    A3: 'Nothing',
    CA: "Closed",
    ET: 'Do NOT Cancel Orders',
    E: 'Once you accept an order, you CANNOT cancel it. Cancelling an order after you accept will result in your account being closed.'
  },
  {
    Q: 'Colors and whites may be washed together.',
    A1: 'True',
    A2: 'False',
    A3: 'Depends',
    CA: "False",
    ET: 'Separate Colors &amp; Whites',
    E: 'Never Wash Colors &amp; Whites Together. Colors and whites should always be separate loads.'
  },
  {
    Q: 'Clothes should be washed in what temperature water?',
    A1: 'Cold',
    A2: 'Warm',
    A3: 'Hot',
    CA: "Cold",
    ET: 'Wash in Cold Water Only',
    E: 'Consumer Reports recommends not washing clothes in hot water. You can save energy washing in cold water. And it gets the job done just the same. Washing in hot water also runs the risk that clothes shrink, fade, or get damaged. Bottom line: always wash in cold water. It saves energy, extends the life of clothes, and gets clothes cleaner. (You may wash clothes in hot water upon customer request.)'
  },
  {
    Q: 'I can wash customer’s clothes using whichever laundry detergent I choose?',
    A1: 'True',
    A2: 'False',
    A3: 'Depends',
    CA: "False",
    ET: 'Use Approved Detergents Only',
    E: 'We require and customers expect that you use one of our approved laundry detergents. They don’t cost any more, and according to Consumer Reports about 60 points separate the top picks from the worst performers. You can find the list of approved detergents is the resources section of the app.'
  },
  {
    Q: 'My account will be closed if I don’t report laundry weight accurately.',
    A1: 'True',
    A2: 'False',
    A3: 'Depends',
    CA: "True",
    ET: 'Report Weight Accurately',
    E: 'Be very careful to report the weight accurately. Customers will verify what you report and if you overcharge them we will lose a customer, your account will be closed, the customer will be refunded, and you will not be paid for the job.'
  },
  {
    Q: 'If I don’t report the laundry weight accurately the customer will be refunded and I will not be paid.',
    A1: 'True',
    A2: 'False',
    A3: 'Depends',
    CA: "True",
    ET: 'Report Weight Accurately',
    E: 'Be very careful to report the weight accurately. Customers will verify what you report and if you overcharge them we will lose a customer, your account will be closed, the customer will be refunded, and you will not be paid for the job.'
  },
  {
    Q: 'All Sudsters have a ranking that determines how many jobs you get access to.',
    A1: 'True',
    A2: 'False',
    A3: 'Sometimes',
    CA: "True",
    ET: 'The Sudster Ranking System',
    E: 'The higher your ranking the more jobs you get access to and the sooner you get access to them. Sudster rankings are determined by: Customer Ratings, Speed, Promptness and Seniority.'
  },
  {
    Q: 'Sudster rankings are determined by:',
    A1: 'Ratings',
    A2: 'Seniority',
    A3: 'Both',
    CA: "Both",
    ET: 'The Sudster Ranking System',
    E: 'The higher your ranking the more jobs you get access to and the sooner you get access to them. Sudster rankings are determined by: Customer Ratings, Speed, Promptness and Seniority.'
  },
  {
    Q: 'What’s the maximum weight of a laundry bag you can deliver to a customer?',
    A1: '10 lbs',
    A2: '15 lbs',
    A3: '25 lbs',
    CA: "15 lbs",
    ET: 'Bag Weight—Maximum 15 LBS.',
    E: 'Keep all bags under 15 pounds. Some customers are elderly or handicapped and heavier bags are too burdensome.'
  },
  {
    Q: 'What kind of surface should you put your scale on before you weigh customer’s laundry?',
    A1: 'Smooth',
    A2: 'Carpet',
    A3: 'Any',
    CA: "Smooth",
    ET: 'Put Scale on Smooth Flat Surface',
    E: 'Did you know that scales aren’t accurate if they sit on carpet or other non-smooth surfaces? It’s true. So make sure your scale is on a smooth flat surface before you weigh.'
  },
  {
    Q: 'If a customer provides 1 bag of laundry you must return to them 1 packed bag of laundry.',
    A1: 'True',
    A2: 'False',
    A3: 'N/A',
    CA: "False",
    ET: '# Bags in does NOT have to Equal # Bags Out',
    E: 'Some Sudsters mistakenly think that if a customer gives you 1 bag of laundry that you have to give them back 1 bag of laundry. Not true! You can split a customer’s laundry up into as many bags as you want. Customers will often stuff bags full of dirty laundry, making them big and burdensome. But you want to make sure to return to them smaller, lighter, easier to handle bags, even if that means that you give them back 4x as many bags as they gave you.'
  },
  {
    Q: 'If a customer is dissatisfied with your work and refunded, you’re not paid for the job.',
    A1: 'True',
    A2: 'False',
    A3: 'Depends',
    CA: "True",
    ET: 'Satisfaction Guaranteed',
    E: 'If a customer is dissatisfied with your work and refunded, you’re not paid for the job.<br><br>The most common causes of customer dissatisfaction and refunds are:<br><br>-Clothes returned wet<br><br>-Clothes picked up or delivered late<br><br>-Clothes returned messy or not folded'
  },
  {
    Q: 'Hypoallergenic detergent is detergent free from dyes, perfumes, scents, and optical brighteners.',
    A1: 'True',
    A2: 'False',
    A3: 'Unsure',
    CA: "True",
    ET: 'Hypoallergenic Detergent',
    E: 'Customers have the option to choose regular or hypoallergenic detergent. If they choose hypoallergenic, you’ll be asked to confirm that you have hypoallergenic detergent before you accept the order. If you don’t have it—cancel!<br><br>Hypoallergenic detergent is free from dyes, perfumes, scents, and optical brighteners. Some customers need hypoallergenic detergent because they have sensitive skin, allergies, or other health issues. So this is not something to mess around with.<br><br>About 20% of customers require hypoallergenic detergent, so it’s worthwhile to have some in supply. The best hypoallergenic detergent on our approved list is Persil Power-Liquid Sensitive Skin. It’s the highest rated by Consumer Reports and is very economical. If you want the option of accepting hypoallergenic orders too, pick up some now. '
  },
  {
    Q: 'If an order requires hypoallergenic detergent and you don’t have it, you can still accept the order.',
    A1: 'True',
    A2: 'False',
    A3: 'Depends',
    CA: "False",
    ET: 'Hypoallergenic Detergent',
    E: 'Customers have the option to choose regular or hypoallergenic detergent. If they choose hypoallergenic, you’ll be asked to confirm that you have hypoallergenic detergent before you accept the order. If you don’t have it, cancel!<br><br>Hypoallergenic detergent is free from dyes, perfumes, scents, and optical brighteners. Some customers need hypoallergenic detergent because they have sensitive skin, allergies, or other health issues. So this is not something to mess around with.<br><br>About 20% of customers require hypoallergenic detergent, so it’s worthwhile to have some in supply. The best hypoallergenic detergent on our approved list is Persil Power-Liquid Sensitive Skin. It’s the highest rated by Consumer Reports and is very economical. If you want the option of accepting hypoallergenic orders too, pick up some now. '
  },
  {
    Q: 'All clothes must be turned right side out before they are folded. Clothes cannot ever be returned inside out.',
    A1: 'True',
    A2: 'False',
    A3: 'Depends',
    CA: "True",
    ET: 'All Clothes Must Be Right Side Out',
    E: 'Before you fold a garment, make sure it\'s right side out. All clothes must be delivered ready-to-wear. Never return clothes inside out.'
  },
  {
    Q: 'You must respond to every customer message.',
    A1: 'True',
    A2: 'False',
    A3: 'Depends',
    CA: "True",
    ET: 'Respond to Every Message',
    E: 'If a customer messages you, you must respond. And you must respond within 1 hour.<br><br>Even if there’s nothing important to say, even if all the practical information has been communicated, you should always send the last message. You can say something like... "Thank you.", "Got It", "OK".<br><br>Again, always respond to a customer message, and always respond within 1 hour.'
  },
  {
    Q: 'You must respond to a customer message within',
    A1: '1 Hour',
    A2: '2 Hours',
    A3: '3 Hours',
    CA: "1 Hour",
    ET: 'Respond to Every Message',
    E: 'If a customer messages you, you must respond. And you must respond within 1 hour.<br><br>Even if there’s nothing important to say, even if all the practical information has been communicated, you should always send the last message. You can say something like... "Thank you.", "Got It", "OK".<br><br>Again, always respond to a customer message, and always respond within 1 hour.'
  }
]

function AnswerSignupTest(Answer) {

  if (SignupTestNumber >= SignupTestArray.length) {

    var Wrong = SignupTestArray.length - SignupTestRight;

    if (Wrong > 3) {
      //Fail
      if (localStorage.getItem("FailedTest") == 'true') {
        //Failed Twice. Run CF to Lock sudster out
        ToggleLoading();

        var Sudster_FailedTest = firebase.functions().httpsCallable('Sudster_FailedTest');
        Sudster_FailedTest().then(function () {
          ToggleLoading();
        });

      } else {
        CFdatabase.doc("Sudsters/" + UserID).update({
          SignupStepNumber: 5
        });
        SignupTestRight = 0;
        SignupTestNumber = 0;
        $("#SignupBreadcrumb6").removeClass("Done");
        $("#SignupDivHTML").html($("#SignupFrame5").html());
        localStorage.setItem("FailedTest", "true");
        PopupAlert("Failed Test", "Sorry, but you did not pass the test. Your answers indicate that you do not have mastery of SudShare's policies and procedures. You can take the test again after you go through the training once more. Don't give up. Keep trying. You can do it!");
      }
    } else {
      //Pass
      ContinueSignUp(6);
    }

  } else {
    if (Answer == false) {
      SignupTestRight--;
      PopupAlert(SignupTestArray[SignupTestNumber - 1].ET, SignupTestArray[SignupTestNumber - 1].E)
    } else {

      $("#SignupTestQuestion").html(SignupTestArray[SignupTestNumber].Q);

      $("#SignupTestAnswerButtonDiv button:nth-child(1)").html(SignupTestArray[SignupTestNumber].A1);
      if (SignupTestArray[SignupTestNumber].CA == SignupTestArray[SignupTestNumber].A1) {
        $("#SignupTestAnswerButtonDiv button:nth-child(1)").attr("onclick", "AnswerSignupTest(true)");
      } else {
        $("#SignupTestAnswerButtonDiv button:nth-child(1)").attr("onclick", "AnswerSignupTest(false)");
      }

      $("#SignupTestAnswerButtonDiv button:nth-child(2)").html(SignupTestArray[SignupTestNumber].A2);
      if (SignupTestArray[SignupTestNumber].CA == SignupTestArray[SignupTestNumber].A2) {
        $("#SignupTestAnswerButtonDiv button:nth-child(2)").attr("onclick", "AnswerSignupTest(true)");
      } else {
        $("#SignupTestAnswerButtonDiv button:nth-child(2)").attr("onclick", "AnswerSignupTest(false)");
      }

      $("#SignupTestAnswerButtonDiv button:nth-child(3)").html(SignupTestArray[SignupTestNumber].A3);
      if (SignupTestArray[SignupTestNumber].CA == SignupTestArray[SignupTestNumber].A3) {
        $("#SignupTestAnswerButtonDiv button:nth-child(3)").attr("onclick", "AnswerSignupTest(true)");
      } else {
        $("#SignupTestAnswerButtonDiv button:nth-child(3)").attr("onclick", "AnswerSignupTest(false)");
      }

      SignupTestRight++;
      SignupTestNumber++;

    }

  }
}

/*function HelpFindBags(Page) {
  if (Page == 1 || Page == null) {
    PopupHTML("<div id='HelpFindBagsDiv'><h1>Confirm Address</h1><p>Please double check that you are at 4701 Falls RD</p><button onclick='HelpFindBags(2)'>Still Don't See Bags</button></div>", true);
  }
  else if (Page == 2) {
    PopupHTML("<div id='HelpFindBagsDiv'><h1>Confirm Location</h1><p>Please double check that you are by the <b>Front Door</b> of <b>Unit B</b></p><button onclick='HelpFindBags(3)'>Still Don't See Bags</button></div>", true);
  }
  else if (Page == 3) {
    PopupHTML("<div id='HelpFindBagsDiv'><h1>Message Customer</h1><p>Please message your customer that you have arrived and are looking for the bags, then wait a few minutes for a response.</p><button onclick='HelpFindBags(3)'>No Response</button><button onclick='HelpFindBags(3)'>No Response</button></div>", true);
  }

}*/

function AddFirstQuestionToTest() {

  $("#SignupTestQuestion").html(SignupTestArray[0].Q);

  $("#SignupTestAnswerButtonDiv button:nth-child(1)").html(SignupTestArray[0].A1);
  if (SignupTestArray[0].CA == SignupTestArray[0].A1) {
    $("#SignupTestAnswerButtonDiv button:nth-child(1)").attr("onclick", "AnswerSignupTest(true)");
  } else {
    $("#SignupTestAnswerButtonDiv button:nth-child(1)").attr("onclick", "AnswerSignupTest(false)");
  }

  $("#SignupTestAnswerButtonDiv button:nth-child(2)").html(SignupTestArray[0].A2);
  if (SignupTestArray[0].CA == SignupTestArray[0].A2) {
    $("#SignupTestAnswerButtonDiv button:nth-child(2)").attr("onclick", "AnswerSignupTest(true)");
  } else {
    $("#SignupTestAnswerButtonDiv button:nth-child(2)").attr("onclick", "AnswerSignupTest(false)");
  }

  $("#SignupTestAnswerButtonDiv button:nth-child(3)").html(SignupTestArray[0].A3);
  if (SignupTestArray[0].CA == SignupTestArray[0].A3) {
    $("#SignupTestAnswerButtonDiv button:nth-child(3)").attr("onclick", "AnswerSignupTest(true)");
  } else {
    $("#SignupTestAnswerButtonDiv button:nth-child(3)").attr("onclick", "AnswerSignupTest(false)");
  }

  SignupTestNumber++;

}

function CalcPickupTime(SetTime) {

  var Day = " Today";
  var DueHour = (new Date().getHours() + 3);

  var DueUnixTime = (SetTime + 10800000);

  if (SetTime != null) {
    DueHour = (new Date(SetTime).getHours() + 3);
  }

  if (DueHour >= 20 || DueHour <= 11) {
    if (DueHour >= 20) {
      Day = " Tomorrow";
      DueUnixTime = (new Date(SetTime + 24 * 60 * 60 * 1000).setHours(11));
    } else {
      DueUnixTime = new Date(SetTime).setHours(11);
    }
    DueHour = 11;
  }

  var TimeSuffix = DueHour >= 12 && DueHour <= 23 ? "pm" : "am";
  DueHour = ((DueHour + 11) % 12 + 1) + TimeSuffix;

  if (new Date().getTime() > DueUnixTime) {
    Day = "Past Due";
    DueHour = "";
  }

  return DueHour + Day;

}

function CalcDueDate(Timestamp, AcceptedTime, WithoutPretext) {

  AcceptedTime = new Date(AcceptedTime);
  var DueString = "Past Due";
  var DaysOfWeek = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

  var TimeDifference = new Date().getTime() - Timestamp.getTime();

  var TimeDiffInDays = TimeDifference / 86400000;

  if (Timestamp.getHours() > 15) {
    TimeDiffInDays--;
    if (AcceptedTime.getHours() > 15 && AcceptedTime.getHours() < 17) {
      TimeDiffInDays++;
    }
  }

  var Pretext = "Deliver by "
  if (TimeDiffInDays <= 0) {
    //In 2 days
    var DueWeekDay = DaysOfWeek[new Date().getDay() + 2]
    DueString = DueWeekDay + ", 8pm";
  }
  if (TimeDiffInDays >= 0) {
    //Tommorow
    DueString = "Tommorow, 8pm";
  }
  if (TimeDiffInDays >= 1) {
    //Today
    DueString = "Today, 8pm";
  }
  if (TimeDiffInDays >= 2) {
    //Today
    DueString = "Past Due";
  }
  if (WithoutPretext) {
    Pretext = "";
  }
  return Pretext + DueString;
}

function WeighWarningPopup() {
  $("#PopupWeighWarn").show();
  $("#PopupWeighWarn").css("opacity", "1");
  $("#PopupWeighWarnInnerDiv").css("margin-top", "-50px").css("opacity", "1");
  $("body").css("overflow", "hidden");
  $("#Screen").css("-webkit-filter", "blur(10px)").css("transform", "scale(1.05)");
  $("header").css("-webkit-filter", "blur(10px)");
}

function HowPayoutsWork() {
  PopupAlert("How Payouts Work", '<span style=\'font-size:13px;margin-bottom:-20px;display:block;\'>' +
    'There are 2 types of Payouts:' +
    '<ol><li>Regular Payouts</li>' +
    '<li>Instant Payouts</li></ol>' +
    '<b>Regular payouts:</b><br> Regular payouts automatically occur the first business day of every week (usually Monday) and deposits ' +
    'your previous week’s earnings. Most banks deposit payouts into your bank account as soon as they are received, though some may take a few extra days to make them available. There is no fee for a regular payout.' +
    '<br><br>' +
    '<b>Instant Payouts:</b><br> You can get an Instant Payout any time, and your earnings are deposited on to your debit card within ' +
    'minutes. There is a small fee for Instant Payout. ' +
    'In order to receive an Instant Payout you must meet 3 requirements:' +
    '<ol style="padding-left:15px;font-size:13px"><li>Your Payout method must be a debit card.</li>' +
    '<li>You must have at least 3 ratings from customers with an average of 4 or above.</li>' +
    '<li>You must have a balance of at least $10.</li></ol>' +
    '</span>')
}

function EHTML(Text) {
  return Text.replace(/&/g, "&amp;").replace(/>/g, "&gt;").replace(/</g, "&lt;").replace(/"/g, "&quot;");
}

function TakePicForPickup() {

  var MonthArray = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JULY", "AUG", "SEP", "OCT", "NOV", "DEC"]

  try {
    Android.TakePicture("OrderPhotos/" + MAOID + "-PickupPhoto", 800, 800, "ShowPickupPicture");
  } catch (e) {}

  try {
    window.webkit.messageHandlers.Sudsters.postMessage({
      task: "TakePicture",
      Path: "OrderPhotos/" + MAOID + "-PickupPhoto",
      Width: "800",
      Height: "800",
      ReturnFunction: "ShowPickupPicture"
    });
  } catch (e) {}

}

function TakePicForDelivery() {

  var MonthArray = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JULY", "AUG", "SEP", "OCT", "NOV", "DEC"]

  try {
    Android.TakePicture("OrderPhotos/" + MAOID + "-DeliveryPhoto", 800, 800, "ShowDeliveryPicture");
  } catch (e) {}

  try {
    window.webkit.messageHandlers.Sudsters.postMessage({
      task: "TakePicture",
      Path: "OrderPhotos/" + MAOID + "-DeliveryPhoto",
      Width: "800",
      Height: "800",
      ReturnFunction: "ShowDeliveryPicture"
    });
  } catch (e) {}

}

function ShowDeliveryPicture(URL) {
  $("#MAODeliveryPicture").attr("src", URL).css("display", "block");
  localStorage.setItem(MAOID + "-DeliveryImage", URL);
}

function ShowPickupPicture(URL) {
  $("#MAOPickupPicture").attr("src", URL).css("display", "block");
  localStorage.setItem(MAOID + "-PickupImage", URL);
}

function CalcDeliveryDeadline(Timestamp, AcceptedTimeStamp, ReturnUnix) {

  var AcceptedHour = AcceptedTimeStamp.getHours();

  var DueString = "Past Due";
  var DaysOfWeek = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

  var TimeDifference = new Date().getTime() - Timestamp.getTime();

  var TimeDiffInDays = TimeDifference / 86400000;

  var DueDateUnix = (new Date(new Date().getTime() + 24 * 60 * 60 * 1000).setHours(20));

  if (Timestamp.getHours() > 15) {
    TimeDiffInDays--;
    DueDateUnix += (24 * 60 * 60 * 1000);
    if (AcceptedHour > 15 && AcceptedHour < 17) {
      TimeDiffInDays++;
      DueDateUnix -= (24 * 60 * 60 * 1000);
    }
  }

  if (TimeDiffInDays <= 0) {
    //In 2 days
    var DueWeekDay = DaysOfWeek[new Date().getDay() + 2]
    DueString = DueWeekDay + ", 8pm";
  }
  if (TimeDiffInDays >= 0) {
    //Tommorow
    DueString = "Tommorow, 8pm";
  }
  if (TimeDiffInDays >= 1) {
    //Today
    DueString = "Today, 8pm";
  }
  if (TimeDiffInDays >= 2) {
    //Today
    DueString = "shortly";
  }

  if (ReturnUnix) {
    return DueDateUnix;
  } else {
    return DueString;
  }
}

function CalcPickupDeadline(ReturnUnix) {

  var TimeUnix = new Date().getTime();

  var Day = " Today";
  var DueHour = (new Date().getHours() + 3);

  var DueUnixTime = (TimeUnix + 10800000);

  if (DueHour >= 20 || DueHour <= 11) {
    if (DueHour >= 20) {
      Day = " Tomorrow";
      DueUnixTime = (new Date(TimeUnix + 24 * 60 * 60 * 1000).setHours(11));
    } else {
      DueUnixTime = new Date().setHours(11);
    }
    DueHour = 11;
  }

  var TimeSuffix = DueHour >= 12 && DueHour <= 23 ? "pm" : "am";
  DueHour = ((DueHour + 11) % 12 + 1) + TimeSuffix;

  if (new Date().getTime() > DueUnixTime) {
    Day = "Past Due";
    DueHour = "";
  }

  if (ReturnUnix) {
    return DueUnixTime;
  } else {
    return DueHour + Day;
  }

}

function ConvertUnixToDeliveryDate(Time) {

  var DaysOfWeek = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
  var DueDate = new Date(Time);
  var ReturnString = "";
  var TimeDifference = Time - new Date().getTime();
  var TimeDiffInDays = TimeDifference / 86400000;

  if (TimeDiffInDays > 0) {
    ReturnString = "Today, 8pm";
  }
  if (TimeDiffInDays > 1) {
    ReturnString = "Tomorrow, 8pm";
  }
  if (TimeDiffInDays > 2) {
    var DueWeekDay = DaysOfWeek[new Date().getDay() + 2]
    ReturnString = DueWeekDay + ", 8pm";
  }

  if (new Date().getTime() > Time) {
    ReturnString = "Past Due";
  }

  return ReturnString;

}

function ConvertUnixToPickupTime(UnixTime) {

  var date = new Date(UnixTime);

  var hours = date.getHours();
  var ampm = hours >= 12 ? 'PM' : 'AM';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  var strTime = hours + '' + ampm;
  var PickupDeadlineDayString = "Today, "
  if (date.getDay() != new Date().getDay()) {
    PickupDeadlineDayString = "Tomorrow, "
  }
  var ReturnString = PickupDeadlineDayString + strTime;

  if (new Date().getTime() > UnixTime) {
    ReturnString = "Past Due";
  }

  return ReturnString;

}